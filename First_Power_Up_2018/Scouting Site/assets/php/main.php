<?php
$database = 'hofstra';
class Team
{
    public $id = null;
    public $name = null;
    public $rank = 0;
    public $matches = 0;
    public $switch = 0;
    public $scale = 0;
    public $exchange = 0;
    public $climb = 0;
    public $aline = 0;
    public $aswitch = 0;
    public $ascale = 0;

    public function setid($v){
        $this->id = $v;
    }

    public function setname($v){
        $this->name = $v;
    }

    public function setrank($v){
        $this->rank = $v;
    }

    public function setmatches($v){
        $this->matches = $v;
    }

    public function setswitch($v){
        $this->switch = $v;
    }

    public function setscale($v){
        $this->scale = $v;
    }

    public function setexchange($v){
        $this->exchange = $v;
    }

    public function setclimb($v){
        $this->climb = $v;
    }

    public function setaline($v){
        $this->aline = $v;
    }

    public function setaswitch($v){
        $this->aswitch = $v;
    }

    public function setascale($v){
        $this->ascale = $v;
    }

    public function getid(){
        return $this->id;
    }

    public function getname(){
        return $this->name;
    }

    public function getrank(){
        return $this->rank;
    }

    public function getmatches(){
        return $this->matches;
    }

    public function getswitch(){
        return $this->switch;
    }

    public function getscale(){
        return $this->scale;
    }

    public function getexchange(){
        return $this->exchange;
    }

    public function getclimb(){
        return $this->climb;
    }

    public function getaline(){
        return $this->aline;
    }

    public function getaswitch(){
        return $this->aswitch;
    }

    public function getascale(){
        return $this->ascale;
    }
}

class Match
{
    public $id = null;
    public $red1 = 0;
    public $red2 = 0;
    public $red3 = 0;
    public $blue1 = 0;
    public $blue2 = 0;
    public $blue3 = 0;
    public $redscore = 0;
    public $bluescore = 0;

    public function setid($v){
        $this->id = $v;
    }

    public function setred1($v){
        $this->red1 = $v;
    }

    public function setred2($v){
        $this->red2 = $v;
    }

    public function setred3($v){
        $this->red3 = $v;
    }

    public function setblue1($v){
        $this->blue1 = $v;
    }

    public function setblue2($v){
        $this->blue2 = $v;
    }

    public function setblue3($v){
        $this->blue3 = $v;
    }

    public function setredscore($v){
        $this->redscore = $v;
    }

    public function setbluescore($v){
        $this->bluescore = $v;
    }

    public function getid(){
        return $this->id;
    }

    public function getred1(){
        return $this->red1;
    }

    public function getred2(){
        return $this->red2;
    }

    public function getred3(){
        return $this->red3;
    }

    public function getblue1(){
        return $this->blue1;
    }

    public function getblue2(){
        return $this->blue2;
    }

    public function getblue3(){
        return $this->blue3;
    }

    public function getredscore(){
        return $this->redscore;
    }

    public function getbluescore(){
        return $this->bluescore;
    }
}

class MatchPlay
{
    public $teamid = null;
    public $match = '0';
    public $teampos = '0';
    public $switch = '0';
    public $scale = '0';
    public $exchange = '0';
    public $climb = '0';
    public $aline = '0';
    public $aswitch = '0';
    public $ascale = '0';
}

function teamid($a, $b) {
    return $a->getid() > $b->getid();
}

function matchsort($a, $b) {
    return $a->teampos > $b->teampos;
}

function teamname($a, $b) {
    return strcmp($a->getname(), $b->getname());
}

function rank($a, $b) {
    return strcmp($a->getrank(), $b->getrank());
}
