<?php
include('/var/www/scouting/assets/php/main.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Match Scouting</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team 514 Scouting</h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main fulltable">
      <div class="justthetable">
        <div class="matchheader">
          <table>
            <tr>
              <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="match.php">View Matches</a></th>
              <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="score.php">Score Match</a></th>
            </tr>
          </table>
        </div>
        <table style="width:100%">
          <tr>
            <th style="width:10%;background-color:#3f51b5;">Match Number</th>
            <th style="width:14%;background-color:#3f51b5;">Winning Alliance</th>
            <th style="width:10%;background-color:#3f51b5;">Red 1</th>
            <th style="width:10%;background-color:#3f51b5;">Red 2</th>
            <th style="width:10%;background-color:#3f51b5;">Red 3</th>
            <th style="width:10%;background-color:#3f51b5;">Blue 1</th>
            <th style="width:10%;background-color:#3f51b5;">Blue 2</th>
            <th style="width:10%;background-color:#3f51b5;">Blue 3</th>
            <th style="width:10%;background-color:#3f51b5;">Red Score</th>
            <th style="width:10%;background-color:#3f51b5;">Blue Score</th>
          </tr>
          <?php
            $teams = array();
            $matches = array();
            $matchplay = array();
            $conn = mysqli_connect("127.0.0.1", "root", "password");
            if ($conn) {
                mysqli_select_db($conn, $database);
                $res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
                if (mysqli_num_rows($res) > 0) {
                    while($row = mysqli_fetch_assoc($res)) {
                        $t = new Team();
                        $t->setid($row['id']);
                        $t->setname($row['name']);
                        array_push($teams, $t);
                    }
                }
                $res = mysqli_query($conn, "SELECT * FROM matchplay ORDER BY id ASC;");
                if (mysqli_num_rows($res) > 0) {
                    while($row = mysqli_fetch_assoc($res)) {
                        foreach ($teams as $t) {
                            if ($t->getid() == $row['id']) {
                                $t->setmatches($t->getmatches() + 1);
                                $t->setswitch($t->getswitch() + $row['switch']);
                                $t->setscale($t->getscale() + $row['scale']);
                                $t->setexchange($t->getexchange() + $row['exchange']);
                                $t->setclimb($t->getclimb() + $row['climb']);
                                $t->setaline($t->getaline() + $row['aline']);
                                $t->setaswitch($t->getaswitch() + $row['aswitch']);
                                $t->setascale($t->getascale() + $row['ascale']);
                            }
                        }
                        $mp = new MatchPlay();
                        $mp->teamid = $row['id'];
                        $mp->match = $row['match'];
                        $mp->teampos = $row['team'];
                        $mp->switch = $row['switch'];
                        $mp->scale = $row['scale'];
                        $mp->exchange = $row['exchange'];
                        $mp->climb = $row['climb'];
                        $mp->aline = $row['aline'];
                        $mp->aswitch = $row['aswitch'];
                        $mp->ascale = $row['ascale'];
                        array_push($matchplay, $mp);
                    }
                }
                $res = mysqli_query($conn, "SELECT * FROM matches ORDER BY id ASC;");
                if (mysqli_num_rows($res) > 0) {
                    while($row = mysqli_fetch_assoc($res)) {
                        $m = new Match();
                        $m->setid($row['id']);
                        $m->setred1($row['red1']);
                        $m->setred2($row['red2']);
                        $m->setred3($row['red3']);
                        $m->setblue1($row['blue1']);
                        $m->setblue2($row['blue2']);
                        $m->setblue3($row['blue3']);
                        $m->setredscore($row['redscore']);
                        $m->setbluescore($row['bluescore']);
                        array_push($matches, $m);
                    }
                }
                usort($teams, "teamid");
                // usort($matches, "teamid");
                foreach ($matches as $m) {
                    $red1 = '0';
                    $red2 = '0';
                    $red3 = '0';
                    $blue1 = '0';
                    $blue2 = '0';
                    $blue3 = '0';
                    foreach ($matchplay as $match) {
                        if (intval($match->match) == intval($m->getid())) {
                            switch (intval($match->teampos)) {
                                case 0:
                                    $red1 = $match->teamid;
                                    break;
                                case 1:
                                    $red2 = $match->teamid;
                                    break;
                                case 2:
                                    $red3 = $match->teamid;
                                    break;
                                case 3:
                                    $blue1 = $match->teamid;
                                    break;
                                case 4:
                                    $blue2 = $match->teamid;
                                    break;
                                case 5:
                                    $blue3 = $match->teamid;
                                    break;
                            }
                        }
                    }
                    echo '<tr><td><a style="text-decoration:underline" href="/matchscore.php?id=' . $m->getid() . '">' . $m->getid() . '</a></td>
                    <td class="' . ($m->getredscore() > $m->getbluescore() ? 'red' : 'blue') . 'alliance">' . ($m->getredscore() > $m->getbluescore() ? 'Red' : 'Blue') . '</td>
                    <td class="redalliance"><a style="text-decoration:underline" href="/team.php?id=' . $m->getred1() . '">' . $m->getred1() . '</td>
                    <td class="redalliance"><a style="text-decoration:underline" href="/team.php?id=' . $m->getred2() . '">' . $m->getred2() . '</td>
                    <td class="redalliance"><a style="text-decoration:underline" href="/team.php?id=' . $m->getred3() . '">' . $m->getred3() . '</td>
                    <td class="bluealliance"><a style="text-decoration:underline" href="/team.php?id=' . $m->getblue1() . '">' . $m->getblue1() . '</td>
                    <td class="bluealliance"><a style="text-decoration:underline" href="/team.php?id=' . $m->getblue2() . '">' . $m->getblue2() . '</td>
                    <td class="bluealliance"><a style="text-decoration:underline" href="/team.php?id=' . $m->getblue3() . '">' . $m->getblue3() . '</td>
                    <td' . ($m->getredscore() > $m->getbluescore() ? ' class="won"' : '') . '>' . $m->getredscore() . '</td>
                    <td' . ($m->getredscore() > $m->getbluescore() ? '' : ' class="won"') . '>' . $m->getbluescore() . '</td></tr>';
                }
            }
          ?>
        </table>
      </div>
    </div>
  </body>
</html>
