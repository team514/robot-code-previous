<?php
include('/var/www/scouting/assets/php/main.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Team <?php echo $_GET['id']; ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team 514 Scouting</h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main">
      <div class="row">
        <div class="col-sm-8">
          <?php
          $teams = array();
          $matchids = array();
          $conn = mysqli_connect("127.0.0.1", "root", "password");
          $team;
          if ($conn) {
              mysqli_select_db($conn, $database);
              $res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
              if (mysqli_num_rows($res) > 0) {
                  while($row = mysqli_fetch_assoc($res)) {
                      $t = new Team();
                      $t->setid($row['id']);
                      $t->setname($row['name']);
                      array_push($teams, $t);
                  }
              }
              $res = mysqli_query($conn, "SELECT * FROM matchplay ORDER BY id ASC;");
              if (mysqli_num_rows($res) > 0) {
                  while($row = mysqli_fetch_assoc($res)) {
                      foreach ($teams as $t) {
                          if ($t->getid() == $row['id']) {
                              $t->setmatches($t->getmatches() + 1);
                              $t->setswitch($t->getswitch() + $row['switch']);
                              $t->setscale($t->getscale() + $row['scale']);
                              $t->setexchange($t->getexchange() + $row['exchange']);
                              $t->setclimb($t->getclimb() + $row['climb']);
                              $t->setaline($t->getaline() + $row['aline']);
                              $t->setaswitch($t->getaswitch() + $row['aswitch']);
                              $t->setascale($t->getascale() + $row['ascale']);
                              if($t->getid() == $_GET['id']){
                                  array_push($matchids, $row['match']);
                              }
                          }
                      }
                  }
              }
          }
          foreach ($teams as $t) {
              if($t->getid() == $_GET['id']){
                  $team = $t;
              }
          }
          $total = sizeof($teams);
          $matches = $team->getmatches();
          $switch = $team->getswitch();
          $scale = $team->getscale();
          $exchange = $team->getexchange();
          $climb = $team->getclimb();
          $aline = $team->getaline();
          $aswitch = $team->getaswitch();
          $ascale = $team->getascale();
          $switchrank = 1;
          $scalerank = 1;
          $exchangerank = 1;
          $climbrank = 1;
          $alinerank = 1;
          $aswitchrank = 1;
          $ascalerank = 1;
          foreach ($teams as $t) {
              if ($t->getid() == $team->getid()) {
                  continue;
              }
              if ($t->getswitch() > $switch) {
                  $switchrank += 1;
              }
              if ($t->getscale() > $scale) {
                  $scalerank += 1;
              }
              if ($t->getexchange() > $exchange) {
                  $exchangerank += 1;
              }
              if ($t->getclimb() > $climb) {
                  $climbrank += 1;
              }
              if ($t->getaline() > $aline) {
                  $alinerank += 1;
              }
              if ($t->getaswitch() > $aswitch) {
                  $aswitchrank += 1;
              }
              if ($t->getascale() > $ascale) {
                  $ascalerank += 1;
              }
          }
          if ($matches > 0) {
              $switch /= $matches;
              $scale /= $matches;
              $exchange /= $matches;
              $climb /= $matches;
              $aline /= $matches;
              $aswitch /= $matches;
              $ascale /= $matches;
          }
          ?>
          <h1>Team <?php echo $team->getid(); ?> - <?php echo $team->getname(); ?></h1>
          <h2>Matches Played: <?php
              for($i = 0; $i < sizeof($matchids); $i++){
                  echo '<a href="matchscore.php?id=' . $matchids[$i] . '">' . $matchids[$i] . '</a>';
                  if($i < (sizeof($matchids) - 1)){
                      echo ', ';
                  }
              }
          ?></h2>
          <table style="width:80%">
            <tr>
              <th style="width:25%">Category</th>
              <th style="width:15%">Score</th>
              <th style="width:40%">Rank (out of <?php echo $total; ?>)</th>
            </tr>
            <tr>
              <th>Switch (Average)</th>
              <th><?php echo $switch; ?></th>
              <th><?php echo $switchrank; ?></th>
            </tr>
            <tr>
              <th>Scale (Average)</th>
              <th><?php echo $scale; ?></th>
              <th><?php echo $scalerank; ?></th>
            </tr>
            <tr>
              <th>Exchange (Average)</th>
              <th><?php echo $exchange; ?></th>
              <th><?php echo $exchangerank; ?></th>
            </tr>
            <tr>
              <th>Climb Total</th>
              <th><?php echo $climb; ?></th>
              <th><?php echo $climbrank; ?></th>
            </tr>
            <tr>
              <th>Auto Cross Line</th>
              <th><?php echo $aline; ?></th>
              <th><?php echo $alinerank; ?></th>
            </tr>
            <tr>
              <th>Auto Switch</th>
              <th><?php echo $aswitch; ?></th>
              <th><?php echo $aswitchrank; ?></th>
            </tr>
            <tr>
              <th>Auto Scale</th>
              <th><?php echo $ascale; ?></th>
              <th><?php echo $ascalerank; ?></th>
            </tr>
          </table>
          <h2>Notes</h2>
          <div class="notes">
            <ul>
              <?php
                $res = mysqli_query($conn, "SELECT * FROM notes WHERE id=" . $_GET['id'] . ";");
                if (mysqli_num_rows($res) > 0) {
                    while($row = mysqli_fetch_assoc($res)) {
                        echo '<li>' . nl2br($row['note']) . '</li>';
                    }
                }
              ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="botpic">
            <img id="botpic" src="/images/<?php echo $_GET['id']; ?>.png" onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';" type="image/png" />
            <button id="button">Rotate</button>
          </div>
        </div>
      </div>
    </div>
    <script>
      var rotated = false;

      document.getElementById('button').onclick = function() {
          var div = document.getElementById('botpic'),
          deg = rotated ? 0 : 90;
          div.style.webkitTransform = 'rotate('+deg+'deg)'; 
          div.style.mozTransform    = 'rotate('+deg+'deg)'; 
          div.style.msTransform     = 'rotate('+deg+'deg)'; 
          div.style.oTransform      = 'rotate('+deg+'deg)'; 
          div.style.transform       = 'rotate('+deg+'deg)'; 
          rotated = !rotated;
      }
    </script>
  </body>
</html>
