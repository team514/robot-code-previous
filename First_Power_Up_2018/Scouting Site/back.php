<?php
include('/var/www/scouting/assets/php/main.php');

$id = 0;

if(isset($_GET['id'])){
  $id = $_GET['id'];
}

$conn = mysqli_connect("localhost", "root", "password");
mysqli_select_db($conn, $database);

$matches = array();

$res = mysqli_query($conn, "SELECT * FROM matches ORDER BY id ASC;");
if (mysqli_num_rows($res) > 0) {
    while($row = mysqli_fetch_assoc($res)) {
        $m = new Match();
        $m->setid($row['id']);
        $m->setred1($row['red1']);
        $m->setred2($row['red2']);
        $m->setred3($row['red3']);
        $m->setblue1($row['blue1']);
        $m->setblue2($row['blue2']);
        $m->setblue3($row['blue3']);
        $m->setredscore($row['redscore']);
        $m->setbluescore($row['bluescore']);
        array_push($matches, $m);
    }
}

$match = null;
foreach ($matches as $m) {
    if($m->id == $id){
        $match = $m;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Final Score</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team 514 Scouting</h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main fulltable">
      <div class="matchheader">
        <table>
          <tr>
            <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="match.php">View Matches</a></th>
            <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="score.php">Score Match</a></th>
          </tr>
        </table>
      </div>
      <h1>Match <?php echo $id; ?></h1>
      <form action="finalscore.php" method="get">
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
          <h4>Red Score: <input type="text" name="redscore" value="<?php echo $match->redscore; ?>" /></h4>
          <h4>Blue Score: <input type="text" name="bluescore" value="<?php echo $match->bluescore; ?>" /></h4>
          <input type="submit" value="Update" />
      </form>
    </div>
    <?php
    if($updated):?>
    <script>
      setTimeout(function () {
        document.getElementById("updated").setAttribute("style", "display:none");
      }, 1500);
    </script>
  <?php endif; ?>
  </body>
</html>

