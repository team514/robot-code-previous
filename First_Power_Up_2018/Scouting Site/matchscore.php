<?php
include('/var/www/scouting/assets/php/main.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Match Scoring</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team 514 Match <?php echo $id; ?></h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main fulltable">
        <div class="matchheader">
          <table>
            <tr>
              <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="match.php">View Matches</a></th>
              <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="score.php">Score Match</a></th>
            </tr>
          </table>
        </div>
        <div class="row">
        <?php
            $matches = array();
            $conn = mysqli_connect("127.0.0.1", "root", "password");
            mysqli_select_db($conn, $database);
            $res = mysqli_query($conn, "SELECT * FROM matches ORDER BY id ASC;");
            if (mysqli_num_rows($res) > 0) {
                while($row = mysqli_fetch_assoc($res)) {
                    $m = new Match();
                    $m->setid($row['id']);
                    $m->setred1($row['red1']);
                    $m->setred2($row['red2']);
                    $m->setred3($row['red3']);
                    $m->setblue1($row['blue1']);
                    $m->setblue2($row['blue2']);
                    $m->setblue3($row['blue3']);
                    $m->setredscore($row['redscore']);
                    $m->setbluescore($row['bluescore']);
                    array_push($matches, $m);
                }
            }
            $m = null;
            foreach ($matches as $match){
                if($match->id == $id){
                    $m = $match;
                    break;
                }
            }
        ?>
        <div class="col-sm-12">
          <h1>Match <?php echo $id; ?></h1>
          <?php
            if($m->redscore > $m->bluescore){
                echo '<h2>Winning Alliance: <span class="redalliance">Red</span></h2>';
                echo '<h2><span class="redalliance wonline">' . $m->redscore . '</span> - <span class="bluealliance">' . $m->bluescore . '</span></h2>';
            }else if($m->redscore < $m->bluescore){
                echo '<h2>Winning Alliance: <span class="bluealliance">Blue</span></h2>';
                echo '<h2><span class="redalliance">' . $m->redscore . '</span> - <span class="bluealliance wonline">' . $m->bluescore . '</span></h2>';
            }else{
                echo '<h2>Winning Alliance: <span class="tie">Tie</span></h2>';
                echo '<h2><span class="redalliance">' . $m->redscore . '</span> - <span class="bluealliance">' . $m->bluescore . '</span></h2>';
            }
          ?>
          <div class="justthetable">
            <table style="width:100%">
              <tr>
                <th style="width:4%">Position</th>
                <th style="width:4%">Team Number</th>
                <th style="width:4%">Switch Cubes</th>
                <th style="width:4%">Scale Cubes</th>
                <th style="width:4%">Exchange Cubes</th>
                <th style="width:4%">Climb</th>
                <th style="width:4%">Auto Cross Line</th>
                <th style="width:4%">Auto Switch Cubes</th>
                <th style="width:4%">Auto Scale Cubes</th>
              </tr>
              <?php
              $teams = array();
              $matchplay = array();
              $res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
              if (mysqli_num_rows($res) > 0) {
                  while($row = mysqli_fetch_assoc($res)) {
                      $t = new Team();
                      $t->setid($row['id']);
                      $t->setname($row['name']);
                      array_push($teams, $t);
                  }
              }
              $res = mysqli_query($conn, "SELECT * FROM matchplay WHERE `match`=" . $id . " ORDER BY team ASC;");
              if (mysqli_num_rows($res) > 0) {
                  while($row = mysqli_fetch_assoc($res)) {
                      foreach ($teams as $t) {
                          if ($t->getid() == $row['id']) {
                              $t->setmatches($t->getmatches() + 1);
                              $t->setswitch($t->getswitch() + $row['switch']);
                              $t->setscale($t->getscale() + $row['scale']);
                              $t->setexchange($t->getexchange() + $row['exchange']);
                              $t->setclimb($t->getclimb() + $row['climb']);
                              $t->setaline($t->getaline() + $row['aline']);
                              $t->setaswitch($t->getaswitch() + $row['aswitch']);
                              $t->setascale($t->getascale() + $row['ascale']);
                          }
                      }
                      $mp = new MatchPlay();
                      $mp->teamid = $row['id'];
                      $mp->match = $row['match'];
                      $mp->teampos = $row['team'];
                      $mp->switch = $row['switch'];
                      $mp->scale = $row['scale'];
                      $mp->exchange = $row['exchange'];
                      $mp->climb = $row['climb'];
                      $mp->aline = $row['aline'];
                      $mp->aswitch = $row['aswitch'];
                      $mp->ascale = $row['ascale'];
                      array_push($matchplay, $mp);
                  }
              }
              usort($teams, "teamid");

              $r1 = false;
              $r2 = false;
              $r3 = false;
              $b1 = false;
              $b2 = false;
              $b3 = false;

              foreach ($matchplay as $team) {
                  switch (intval($team->teampos)) {
                      case 0:
                          $r1 = true;
                          break;
                      case 1:
                          $r2 = true;
                          break;
                      case 2:
                          $r3 = true;
                          break;
                      case 3:
                          $b1 = true;
                          break;
                      case 4:
                          $b2 = true;
                          break;
                      case 5:
                          $b3 = true;
                          break;
                  }
              }

              if(!$r1) {
                  $mp = new MatchPlay();
                  $mp->teamid = $m->red1;
                  $mp->match = $id;
                  $mp->teampos = 0;
                  array_push($matchplay, $mp);
              }

              if(!$r2) {
                  $mp = new MatchPlay();
                  $mp->teamid = $m->red2;
                  $mp->match = $id;
                  $mp->teampos = 1;
                  array_push($matchplay, $mp);
              }

              if(!$r3) {
                  $mp = new MatchPlay();
                  $mp->teamid = $m->red3;
                  $mp->match = $id;
                  $mp->teampos = 2;
                  array_push($matchplay, $mp);
              }

              if(!$b1) {
                  $mp = new MatchPlay();
                  $mp->teamid = $m->blue1;
                  $mp->match = $id;
                  $mp->teampos = 3;
                  array_push($matchplay, $mp);
              }

              if(!$b2) {
                  $mp = new MatchPlay();
                  $mp->teamid = $m->blue2;
                  $mp->match = $id;
                  $mp->teampos = 4;
                  array_push($matchplay, $mp);
              }

              if(!$b3) {
                  $mp = new MatchPlay();
                  $mp->teamid = $m->blue3;
                  $mp->match = $id;
                  $mp->teampos = 5;
                  array_push($matchplay, $mp);
              }

              usort($matchplay, "matchsort");

              foreach ($matchplay as $team) {
                  $pos = '';
                  $color = '';
                  switch (intval($team->teampos)) {
                      case 0:
                          $pos = 'Red1';
                          $color = 'redalliance';
                          break;
                      case 1:
                          $pos = 'Red2';
                          $color = 'redalliance';
                          break;
                      case 2:
                          $pos = 'Red3';
                          $color = 'redalliance';
                          break;
                      case 3:
                          $pos = 'Blue1';
                          $color = 'bluealliance';
                          break;
                      case 4:
                          $pos = 'Blue2';
                          $color = 'bluealliance';
                          break;
                      case 5:
                          $pos = 'Blue3';
                          $color = 'bluealliance';
                          break;
                  }
                  echo '<tr><td><span class="' . $color . '">' . $pos . '</span></td><td>' . $team->teamid . '</td><td>' . $team->switch . '</td><td>' . $team->scale . '</td><td>' . $team->exchange . '</td><td>' . $team->climb . '</td><td>' . $team->aline . '</td><td>' . $team->aswitch . '</td><td>' . $team->ascale . '</td></tr>';
              }
              ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
