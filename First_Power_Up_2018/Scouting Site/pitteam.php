<?php
include('/var/www/scouting/assets/php/main.php');

$id = 0;

if(isset($_GET['id']) && $_GET['id'] !== ''){
  $id = $_GET['id'];
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Team <?php echo $_GET['id']; ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team <?php echo $id; ?></h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main">
      <div class="row">
        <div class="col-sm-8">
          <?php
          $teams = array();
          $matchids = array();
          $conn = mysqli_connect("127.0.0.1", "root", "password");
          $team;
          mysqli_select_db($conn, $database);
          $res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
          if (mysqli_num_rows($res) > 0) {
              while($row = mysqli_fetch_assoc($res)) {
                  $t = new Team();
                  $t->setid($row['id']);
                  $t->setname($row['name']);
                  array_push($teams, $t);
              }
          }
          $res = mysqli_query($conn, "SELECT * FROM matchplay ORDER BY id ASC;");
          if (mysqli_num_rows($res) > 0) {
              while($row = mysqli_fetch_assoc($res)) {
                  foreach ($teams as $t) {
                      if ($t->getid() == $row['id']) {
                          $t->setmatches($t->getmatches() + 1);
                          $t->setswitch($t->getswitch() + $row['switch']);
                          $t->setscale($t->getscale() + $row['scale']);
                          $t->setexchange($t->getexchange() + $row['exchange']);
                          $t->setclimb($t->getclimb() + $row['climb']);
                          $t->setaline($t->getaline() + $row['aline']);
                          $t->setaswitch($t->getaswitch() + $row['aswitch']);
                          $t->setascale($t->getascale() + $row['ascale']);
                          if($t->getid() == $_GET['id']){
                              array_push($matchids, $row['match']);
                          }
                      }
                  }
              }
          }
          foreach ($teams as $t) {
              if($t->getid() == $_GET['id']){
                  $team = $t;
              }
          }
          ?>
          <h1>Team <?php echo $team->getid(); ?> - <?php echo $team->getname(); ?></h1>
          <h2>Notes</h2>
          <?php
            $notes = "";
            $res = mysqli_query($conn, "SELECT * FROM notes WHERE id=" . $_GET['id'] . ";");
            if (mysqli_num_rows($res) > 0) {
                while($row = mysqli_fetch_assoc($res)) {
                    $notes .= '&#10;' . $row['note'];
                }
            }
          ?>
          <div class="notes">
            <form action="pitteampost.php" method="get">
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <textarea cols='60' rows='8' name="notes"><?php echo $notes; ?></textarea>
                <input type="submit" value="Update" />
            </form>
            <form action="uploadimage.php" method="post" enctype="multipart/form-data">
                Select image to upload:
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" value="Upload Image" name="submit">
            </form>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="botpic">
            <img id="botpic" src="/images/<?php echo $_GET['id']; ?>.png" onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';" type="image/png" />
            <button id="button">Rotate</button>
          </div>
        </div>
      </div>
    </div>
    <script>
      var rotated = false;

      document.getElementById('button').onclick = function() {
          var div = document.getElementById('botpic'),
          deg = rotated ? 0 : 90;
          div.style.webkitTransform = 'rotate('+deg+'deg)'; 
          div.style.mozTransform    = 'rotate('+deg+'deg)'; 
          div.style.msTransform     = 'rotate('+deg+'deg)'; 
          div.style.oTransform      = 'rotate('+deg+'deg)'; 
          div.style.transform       = 'rotate('+deg+'deg)'; 
          rotated = !rotated;
      }
    </script>
  </body>
</html>
