<?php
include('/var/www/scouting/assets/php/main.php');

$sortby = '';
if (isset($_GET['sortby']) && $_GET['sortby'] != '') {
    $sortby = $_GET['sortby'];
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Scouting</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <script src="/assets/js/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team 514 Scouting</h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main fulltable">
      <div class="justthetable">
        <div class="sort">
          <h4>Sort By
            <select id="sortby">
              <option value="0" <?php echo ($sortby == '' || $sortby == '0') ? 'selected="selected"' : '' ?>>Team Number</option>
              <option value="1" <?php echo ($sortby == '1') ? 'selected="selected"' : '' ?>>Team Name</option>
              <option value="2" <?php echo ($sortby == '2') ? 'selected="selected"' : '' ?>>Rank</option>
            </select>
          </h4>
        </div>
        <table>
          <tr>
            <th style="width:10%">Team Number</th>
            <th style="width:15%">Team Name</th>
            <th style="width:6%">Rank</th>
            <th style="width:10%">Matches Played</th>
            <th style="width:10%">Avg Switch</th>
            <th style="width:10%">Avg Scale</th>
            <th style="width:10%">Avg Exchange</th>
            <th style="width:10%">Climb</th>
            <th style="width:6%">A. Line</th>
            <th style="width:6%">A. Switch</th>
            <th style="width:6%">A. Scale</th>
          </tr>
          <?php
            $teams = array();
            $conn = mysqli_connect("127.0.0.1", "root", "password");
            if ($conn) {
                mysqli_select_db($conn, $database);
                $res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
                if (mysqli_num_rows($res) > 0) {
                    while ($row = mysqli_fetch_assoc($res)) {
                        $t = new Team();
                        $t->setid($row['id']);
                        $t->setname($row['name']);
                        array_push($teams, $t);
                    }
                }
                $res = mysqli_query($conn, "SELECT * FROM matchplay ORDER BY id ASC;");
                if (mysqli_num_rows($res) > 0) {
                    while ($row = mysqli_fetch_assoc($res)) {
                        foreach ($teams as $t) {
                            if ($t->getid() == $row['id']) {
                                $t->setmatches($t->getmatches() + 1);
                                $t->setswitch($t->getswitch() + $row['switch']);
                                $t->setscale($t->getscale() + $row['scale']);
                                $t->setexchange($t->getexchange() + $row['exchange']);
                                $t->setclimb($t->getclimb() + $row['climb']);
                                $t->setaline($t->getaline() + $row['aline']);
                                $t->setaswitch($t->getaswitch() + $row['aswitch']);
                                $t->setascale($t->getascale() + $row['ascale']);
                            }
                        }
                    }
                }
                switch ($sortby) {
                  case '1':
                    usort($teams, "teamname");
                    break;
                  case '2':
                    usort($teams, "rank");
                    // no break
                  default:
                    usort($teams, "teamid");
                    break;
                }
                foreach ($teams as $t) {
                    echo '<tr><td><a style="text-decoration:underline" href="team.php?id=' . $t->getid() . '">' . $t->getid() . '</a></td><td><a style="text-decoration:underline" href="team.php?id=' . $t->getid() . '">' . $t->getname() . '</a></td><td>' . $t->getrank() . '</td><td>' . $t->getmatches() . '</td><td>' . ($t->getmatches() > 0 ? ($t->getswitch() / ((double)$t->getmatches())) : 0) . '</td><td>' . ($t->getmatches() > 0 ? ($t->getscale() / ((double)$t->getmatches())) : 0) . '</td><td>' . ($t->getmatches() > 0 ? ($t->getexchange() / ((double)$t->getmatches())) : 0) . '</td><td>' . ($t->getmatches() > 0 ? ($t->getclimb() / ((double)$t->getmatches())) : 0) . '</td><td>' . ($t->getmatches() > 0 ? ($t->getaline() / ((double)$t->getmatches())) : 0) . '</td><td>' . ($t->getmatches() > 0 ? ($t->getaswitch() / ((double)$t->getmatches())) : 0) . '</td><td>' . ($t->getmatches() > 0 ? ($t->getascale() / ((double)$t->getmatches())) : 0) . '</td></tr>';
                }
            }
          ?>
        </table>
      </div>
    </div>
    <script type="text/javascript">
      $(function() {
        $("#sortby").change(function() {
          window.location='/?sortby=' + this.value;
        });
      });
    </script>
  </body>
</html>
