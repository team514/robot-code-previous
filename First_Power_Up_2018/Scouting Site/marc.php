<?php
include('/var/www/scouting/assets/php/main.php');

$matches = array();

$conn = mysqli_connect("localhost", "root", "password");
mysqli_select_db($conn, $database);
$res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
if (mysqli_num_rows($res) > 0) {
    while($row = mysqli_fetch_assoc($res)) {
        $t = new Team();
        $t->setid($row['id']);
        $t->setname($row['name']);
        array_push($teams, $t);
    }
}
$res = mysqli_query($conn, "SELECT * FROM matches ORDER BY id ASC;");
if (mysqli_num_rows($res) > 0) {
    while($row = mysqli_fetch_assoc($res)) {
        $m = new Match();
        $m->setid($row['id']);
        $m->setred1($row['red1']);
        $m->setred2($row['red2']);
        $m->setred3($row['red3']);
        $m->setblue1($row['blue1']);
        $m->setblue2($row['blue2']);
        $m->setblue3($row['blue3']);
        $m->setredscore($row['redscore']);
        $m->setbluescore($row['bluescore']);
        array_push($matches, $m);
    }
}
$pos = array();
array_push($pos, 0);
array_push($pos, 1);
array_push($pos, 2);
array_push($pos, 3);
array_push($pos, 4);
array_push($pos, 5);
$q = "INSERT INTO matchplay (id,switch, scale, exchange, climb, aline, aswitch, ascale, `match`, team) VALUES ";
for($i = 0; $i < sizeof($matches); $i++){
    $m = $matches[$i];
    foreach($pos as $p){
        $team = 0;
        switch ($p) {
          case 0:
            $team = $m->red1;
            break;
          case 1:
            $team = $m->red2;
            break;
          case 2:
            $team = $m->red3;
            break;
          case 3:
            $team = $m->blue1;
            break;
          case 4:
            $team = $m->blue2;
            break;
          case 5:
            $team = $m->blue3;
            break;
        }
        $q .= "(" . $team . ",0,0,0,0,0,0,0," . $m->id . "," . $p . ")";
        if($i < (sizeof($matches) - 1)){
            $q .= ",";
        }else if($p < 5){
            $q .= ',';
        }
    }
}
$q .= ";";
die($q);
mysqli_query($conn, $q);

