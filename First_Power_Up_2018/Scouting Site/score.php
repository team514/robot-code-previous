<?php
include('/var/www/scouting/assets/php/main.php');

$id = 0;
$team = 0;

if(isset($_GET['id'])){
  $id = $_GET['id'];
}
if(isset($_GET['team'])){
  $team = $_GET['team'];
}

$updated = false;
if(isset($_GET['updated']) && $_GET['updated'] !== ''){
  $updated = $_GET['updated'] == 1;
}

$pos = '';

$conn = mysqli_connect("127.0.0.1", "root", "password");
mysqli_select_db($conn, $database);

$teams = array();
$matches = array();
$matchplay = array();

$res = mysqli_query($conn, "SELECT * FROM teams ORDER BY id ASC;");
if (mysqli_num_rows($res) > 0) {
    while($row = mysqli_fetch_assoc($res)) {
        $t = new Team();
        $t->setid($row['id']);
        $t->setname($row['name']);
        array_push($teams, $t);
    }
}
$res = mysqli_query($conn, "SELECT * FROM matches ORDER BY id ASC;");
if (mysqli_num_rows($res) > 0) {
    while($row = mysqli_fetch_assoc($res)) {
        $m = new Match();
        $m->setid($row['id']);
        $m->setred1($row['red1']);
        $m->setred2($row['red2']);
        $m->setred3($row['red3']);
        $m->setblue1($row['blue1']);
        $m->setblue2($row['blue2']);
        $m->setblue3($row['blue3']);
        $m->setredscore($row['redscore']);
        $m->setbluescore($row['bluescore']);
        array_push($matches, $m);
    }
}
$res = mysqli_query($conn, "SELECT * FROM matchplay ORDER BY id ASC;");
if (mysqli_num_rows($res) > 0) {
    while($row = mysqli_fetch_assoc($res)) {
        foreach ($teams as $t) {
            if ($t->getid() == $row['id']) {
                $t->setmatches($t->getmatches() + 1);
                $t->setswitch($t->getswitch() + $row['switch']);
                $t->setscale($t->getscale() + $row['scale']);
                $t->setexchange($t->getexchange() + $row['exchange']);
                $t->setclimb($t->getclimb() + $row['climb']);
                $t->setaline($t->getaline() + $row['aline']);
                $t->setaswitch($t->getaswitch() + $row['aswitch']);
                $t->setascale($t->getascale() + $row['ascale']);
            }
        }
        $mp = new MatchPlay();
        $mp->teamid = $row['id'];
        $mp->match = $row['match'];
        $mp->teampos = $row['team'];
        $mp->switch = $row['switch'];
        $mp->scale = $row['scale'];
        $mp->exchange = $row['exchange'];
        $mp->climb = $row['climb'];
        $mp->aline = $row['aline'];
        $mp->aswitch = $row['aswitch'];
        $mp->ascale = $row['ascale'];
        array_push($matchplay, $mp);
    }
}

$match = null;
foreach ($matches as $m) {
    if($m->id == $id){
        $match = $m;
    }
}

$mplay = null;
foreach ($matchplay as $mp){
  if($mp->teamid == $team && $mp->match == $id){
    $mplay = $mp;
  }
}

if($mplay == null){
  $mplay = new MatchPlay();
}

$red1 = 0;
$red2 = 0;
$red3 = 0;
$blue1 = 0;
$blue2 = 0;
$blue3 = 0;

$teampos = 0;

if(isset($match)){
    $red1 = $match->red1;
    $red2 = $match->red2;
    $red3 = $match->red3;
    $blue1 = $match->blue1;
    $blue2 = $match->blue2;
    $blue3 = $match->blue3;
    if($team == $red1){
      $pos = 'Red 1';
      $teampos = 0;
    }
    if($team == $red2){
      $pos = 'Red 2';
      $teampos = 1;
    }
    if($team == $red3){
      $pos = 'Red 3';
      $teampos = 2;
    }
    if($team == $blue1){
      $pos = 'Blue 1';
      $teampos = 3;
    }
    if($team == $blue2){
      $pos = 'Blue 2';
      $teampos = 4;
    }
    if($team == $blue3){
      $pos = 'Blue 3';
      $teampos = 5;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Match Scoring</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
    <link rel="manifest" href="/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-grid.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <h1 style="text-align: center;">Team 514 Scouting</h1>
    <div class="navigation row">
      <div class="col-sm-4">
        <a href="/">
          <div class="left">
            <h3>Home</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/pit.php">
          <div class="center">
            <h3>Pit Scouting</h3>
          </div>
        </a>
      </div>
      <div class="col-sm-4">
        <a href="/match.php">
          <div class="right">
            <h3>Match Scouting</h3>
          </div>
        </a>
      </div>
    </div>
    <div class="main fulltable">
      <div class="matchheader">
        <table>
          <tr>
            <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="match.php">View Matches</a></th>
            <th style="width:50%;background-color:#3f51b5;text-align:center;"><a href="score.php">Score Match</a></th>
          </tr>
        </table>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php
            if($updated){
              echo '<h1 id="updated" style="color:#4CAF50;">Updated!</h1>';
            }
            echo '<h1>Match <select onChange="window.document.location.href=\'score.php?id=\' + this.options[this.selectedIndex].value;"><option value="">Select Match</option>';
            foreach ($matches as $m) {
                $selected = $m->id == $id ? ' selected' : '';
                echo '<option value="' . $m->id . '"' . $selected . '>' . $m->id . '</option>';
            }
            echo '</select></h1>';
          ?>
        </div>
      </div>
      <?php if(isset($match) && $team == 0): ?>
      <div class="matchteam row">
        <div class="col-sm-4 team red">
            <a href="score.php?id=<?php echo $id; ?>&team=<?php echo $red1; ?>"><?php echo $red1; ?></a>
        </div>
        <div class="col-sm-4 team red">
            <a href="score.php?id=<?php echo $id; ?>&team=<?php echo $red2; ?>"><?php echo $red2; ?></a>
        </div>
        <div class="col-sm-4 team red">
            <a href="score.php?id=<?php echo $id; ?>&team=<?php echo $red3; ?>"><?php echo $red3; ?></a>
        </div>
        <div class="col-sm-4 team blue">
          <a href="score.php?id=<?php echo $id; ?>&team=<?php echo $blue1; ?>"><?php echo $blue1; ?></a>
        </div>
        <div class="col-sm-4 team blue">
          <a href="score.php?id=<?php echo $id; ?>&team=<?php echo $blue2; ?>"><?php echo $blue2; ?></a>
        </div>
        <div class="col-sm-4 team blue">
          <a href="score.php?id=<?php echo $id; ?>&team=<?php echo $blue3; ?>"><?php echo $blue3; ?></a>
        </div>
        <div class="col-sm-12 team yellow">
          <a href="finalscore.php?id=<?php echo $id; ?>" style="color: black;">Final Score</a>
        </div>
      </div>
      <?php endif; if(isset($match) && $team != 0): ?>
      <div>
        <h1>Team <?php echo $team; ?> (<?php echo $pos; ?>)</h1>
        <form action="scorepost.php" method="get">
          <input type="hidden" name="match" value="<?php echo $match->id; ?>" />
          <input type="hidden" name="teamid" value="<?php echo $team; ?>" />
          <input type="hidden" name="teampos" value="<?php echo $teampos; ?>" />
          <h2>Auto Line: <input type="text" placeholder="Auto Line" name="aline" value="<?php echo $mplay->aline; ?>"/></h2>
          <h2>Auto Switch: <input type="text" placeholder="Auto Switch" name="aswitch" value="<?php echo $mplay->aswitch; ?>"/></h2>
          <h2>Auto Scale: <input type="text" placeholder="Auto Scale" name="ascale" value="<?php echo $mplay->ascale; ?>"/></h2>
          <h2>Switch: <input type="text" placeholder="Switch" name="switch" value="<?php echo $mplay->switch; ?>"/></h2>
          <h2>Scale: <input type="text" placeholder="Scale" name="scale" value="<?php echo $mplay->scale; ?>"/></h2>
          <h2>Exchange: <input type="text" placeholder="Exchange" name="exchange" value="<?php echo $mplay->exchange; ?>"/></h2>
          <h2>Climb: <input type="text" placeholder="Climb" name="climb" value="<?php echo $mplay->climb; ?>"/></h2>
          <input type="submit" value="Update"/>
        </form>
      </div>
      <?php endif; ?>
    </div>
    <?php
    if($updated):?>
    <script>
      setTimeout(function () {
        document.getElementById("updated").setAttribute("style", "display:none");
      }, 1500);
    </script>
  <?php endif; ?>
  </body>
</html>

