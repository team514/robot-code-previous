/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {

	//Auto Variables
	
	public static final double wheelCirc = 18.849;
	public static final double tickCount = 127.0;
	public static final double tickFoot = 80.853;
	public static final double Distance = 970.236;
	
	//Auto Drive Distances
	
	public static final double Line = 10.0;
	public static final double Center0 = 3.0;
	public static final double CenterL2 = 3.5;
	public static final double CenterR2 = 4.5;
	public static final double Center4 = 6.9;
	public static final double Scale0 = 26.25;
	public static final double Scale2 = 1.0;
	public static final double NearSwitch0 = 14.0;
	public static final double NearSwitch2 = 1.5;
	public static final double SwitchOpposite0 = 19.0;
	public static final double SwitchOpposite2 = 16.5;
	public static final double SwitchOpposite4 = 5.0;
	public static final double SwitchOpposite6 = 2.0;
	
	//Auto Arm Position and Wrist Position
	public static final double armAutoLow = 900.0;
	public static final double armAutoHigh = 930.0;
	public static final double wristAutoScaleLow = 450.0;
	public static final double wristAutoScaleHigh = 460.0;
	public static final double wristAutoSwitchLow = 500.0;
	public static final double wristAutoSwitchHigh = 510.0;

	
	//Coerce to Range Variables
	public static final double C2R_inputMin = -30.0;
	public static final double C2R_inputMax = 30.0;
	public static final double C2R_gyroInputMin = -15.0;
	public static final double C2R_gyroInputMax = 15.0;
	public static final double C2R_outputMin = -0.5;
	public static final double C2R_outputMax = 0.5;
	public static final double C2R_inputWristMin = 510.0;
	public static final double C2R_inputWristMax = 570.0;
	public static final double C2R_outputWristMin = -1.0;
	public static final double C2R_outputWristMax = 1.0;

	        
	//public static final double Distance = 242.559;
	public static final double Rotation = 534.473;
    public static final double autoDriveSpeed = 0.8;
    public static final double autoStopDistance = 1.5;
    public static final double autoTurnSpeed = 0.6;
	
	//Constants
	public static final double cubeSpeed = 0.75;
	
	//This is DriveUtil
	
	//Motor Mapping
    public static final NeutralMode motorNeutralMode = NeutralMode.Brake;
    public static final int leftDriveMaster = 2;
    public static final int leftDriveSlave = 0;
    public static final int rightDriveMaster = 1;
    public static final int rightDriveSlave = 3;
   
    //Sensor Mapping
    
    public static final int leftEncoderForward = 0;
    public static final int leftEncoderReverse = 1;
    public static final int rightEncoderForward = 2;
    public static final int rightEncoderReverse = 3;
    
    //This is CubeUtil
    
    //Motor Mapping
    public static final int cubeMotorLeft = 0;
    public static final int cubeMotorRight = 1;
    public static final int cubeMotorArm = 3;
    public static final int cubeMotorWrist= 4;
    
    //Pneumatics
    //public static final int forward = 0;
    //public static final int reverse = 1;
    public static final int dsChannel = 3;
    public static final int brakeChannel = 2;
    
    //Sensors
    public static final int cubeLimitSwitch = 4;
    public static final int cubeArmPot = 0;
    public static final int cubeWristPot = 1;
    
    
    //This is ClimbUtil
    
    //Motors
    public static final int climbMotors = 2;
    public static final double climbUpSpeed = 1.0;
    public static final double climbDownSpeed = -1.0;
    //Sensors
    //public static final int upperLimit = 6;
    //public static final int lowerLimit = 5;

    //Arm and Wrist Util Variables go here.
  		public static final double armSpeed = -0.5;
  		public static final double wristSpeed = 0.35;
  		
  		/* Inverted these values below...  Keep for FYI only.
  		 * public static final double armUpperLimit = 70.0;
  		 * public static final double armUpperConstraint = 125.0;
  		 * public static final double armLowerConstraint = 215.0;
  		 * public static final double armLowerLimit = 223.0;
  		 * 
  		 * public static final double wristUpperLimit = 47.0;
  		 * public static final double wristUpperConstraint = 52.0;
  		 * public static final double wristLowerConstraint = 50.0;
  		 * public static final double wristLowerLimit = 55.0;
  		 */

  		public static final double armUpperLimit = 930.0;
  		public static final double armUpperConstraint = 875.0;
  		public static final double armLowerConstraint = 795.0;
  		public static final double armLowerLimit = 777.0;
  		
  		public static final double wristUpperLimit = 515.0;
  		public static final double wristUpperConstraint = 500.0;
  		public static final double wristLowerConstraint = 490.0;
  		public static final double wristLowerLimit = 450.0;

  		public static final double wristDownUpperConstraint = 500.0;
  		public static final double wristDownLowerConstraint = 490.0;
  		
  		public static final double hookWristLowerLimit = 385.0;
  		public static final double hookWristUpperLimit = 393.0;
  		
  		public static final double sophiaWristLowerLimit = 510.0;
  		public static final double sophiaWristUpperLimit = 515.0;
  		

  	
  	//Left Joystick Button Map goes here...
  	
    //Right Joystick Button Map goes here...
  		public static final int DriveArcade = 4;
  		public static final int DriveTank = 3;
  		public static final int SophiaButton = 12;
  		
  	//Controller Button Map goes here...
  		
  	//LogiTech F310 Button Mapping X/D Switch = D, Direct Input
  		//Axis
  		public static final int kLeftXAxisNum = 0;
  		public static final int kLeftYAxisNum = 1;
  		public static final int kRightXAxisNum = 2;
  		public static final int kRightYAxisNum = 3;
  		
  		//For DPad, use controller.getPOV();
  		//public static final int kDPadXAxisNum = 5;
  		//public static final int kDPadYAxisNum = 6;
  		
  		public static final int kXButtonNum = 1;
  		public static final int kAButtonNum = 2;
  		public static final int kBButtonNum = 3;
  		public static final int kYButtonNum = 4;
  		public static final int kLeftBumperNum = 5;
  		public static final int kRightBumperNum = 6;
  		public static final int kLeftTriggerNum = 7;
  		public static final int kRightTriggerNum = 8;
  		public static final int kBackButtonNum = 9;
  		public static final int kStartButtonNum = 10;
  		public static final int kLeftStickButtonNum = 11;
  		public static final int kRightStickButtonNum = 12;
}
