/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.commands.AutoDrive;
import frc.robot.commands.AutoFun;
import frc.robot.commands.AutoLine;
import frc.robot.commands.AutoScore;
import frc.robot.subsystems.ArmUtil;
import frc.robot.subsystems.AutoUtil;
import frc.robot.subsystems.AutoUtil.AutoMode;
import frc.robot.subsystems.AutoUtil.StartLocation;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.CubeUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.States;
import frc.robot.subsystems.WristUtil;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.properties file in the
 * project.
 */
public class Robot extends TimedRobot {
	
	public static final DriveUtil driveUtil = new DriveUtil();
	public static final CubeUtil cubeUtil = new CubeUtil();
	public static final ClimbUtil climbUtil = new ClimbUtil();
	public static final AutoUtil autoUtil = new AutoUtil();
	public static final ArmUtil armUtil = new ArmUtil();
	public static final WristUtil wristUtil = new WristUtil();
	public static final States states = new States();
	public static OI oi;

	Command autoLine, autoDrive, autoScore, autoFun;
	SendableChooser<String> m_chooser = new SendableChooser<>();
	SendableChooser<String> sl_chooser = new SendableChooser<>();
	boolean autoStarted;
	String gameData;
	int loc;
	DriverStation.Alliance color;

	UsbCamera cam1;
	
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		oi = new OI();
		m_chooser.addDefault("Cross Line", "Default");
		m_chooser.addObject("Score Mode", "ScoreSwitch");
		m_chooser.addObject("No Score Mode", "NoScore");
		m_chooser.addObject("Fun Times", "Fun");
		
		sl_chooser.addDefault("StartLocationDefault", "Auto");
		sl_chooser.addObject("Left", "Left");
		sl_chooser.addObject("Center", "Center");
		sl_chooser.addObject("Right", "Right");
		
		SmartDashboard.putData("Auto Mode: ", m_chooser);		
		SmartDashboard.putData("Start Location: ", sl_chooser);
		
		cam1 = CameraServer.getInstance().startAutomaticCapture();
		
		climbUtil.setClimb(false);
		
	}

	/**
	 * This function is called once each time the robot enters Disabled mode.
	 * You can use it to reset any subsystem information you want to clear when
	 * the robot is disabled.
	 */
	@Override
	public void disabledInit() {

	}

	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString code to get the auto name from the text box below the Gyro
	 *
	 * <p>You can add additional auto modes by adding additional commands to the
	 * chooser code above (like the commented example) or additional comparisons
	 * to the switch structure below with additional strings & commands.
	 */
	@Override
	public void autonomousInit() {
		// First we must get the game data to determine where are targets are
		autoStarted = false;
		loc = DriverStation.getInstance().getLocation();
		gameData = DriverStation.getInstance().getGameSpecificMessage();
		color = DriverStation.getInstance().getAlliance();
		
		autoLine = new AutoLine(loc);
		autoDrive = new AutoDrive(loc);
		autoScore = new AutoScore();
		autoFun = new AutoFun();
		
		driveUtil.resetEncoders();
		driveUtil.resetGyro();
		
		if(gameData != null) {
			autoUtil.setGameData(gameData);
		}
		//Then we get the command with our starting position
		//m_autonomousCommand = m_chooser.getSelected();
		autoUtil.setAutoMode(AutoMode.valueOf(m_chooser.getSelected()));
		autoUtil.setStartLocation(StartLocation.valueOf(sl_chooser.getSelected()));
	}

	/**
	 * This function is called periodically during autonomous.
	 */
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
		updateStatus();
		
		if(gameData == null) {
			gameData = DriverStation.getInstance().getGameSpecificMessage();
		}else{
			autoUtil.setGameData(gameData);
		}

		if((autoUtil.getAutoMode() != null) && 
			(autoUtil.getStartLocation() != null) && 
			(autoUtil.getStringGameData() != null)){
			switch(autoUtil.getAutoMode()){
				case Fun:
					if(!autoStarted){
						autoFun.start();
						autoStarted = true;
					}
					break;
				case Default:
					if(!autoStarted){
						autoLine.start();
						autoStarted = true;
					}
					break;
				case ScoreSwitch:
					if(!autoStarted){
						autoDrive.start();
						autoStarted = true;
					}else{
						if(autoUtil.getAutoDriveDone()){
							if((!autoUtil.getAutoScoreDone()) &&
							  (!autoScore.isRunning())){
								autoScore.start();
							}
						}
					}
					break;
				case NoScore:
					if(!autoStarted){
						autoDrive.start();
						autoStarted = true;
					}
					break;	
					default:
						break;
			}
		}	
	}

	@Override
	public void teleopInit() {
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		// this line or comment it out.
		if(autoLine != null){
			autoLine.cancel();
		}
		if(autoDrive != null){
			autoDrive.cancel();
		}
		if(autoScore != null){
			autoScore.cancel();
		}

		
		driveUtil.resetEncoders();
		driveUtil.resetGyro();
	}

	/**
	 * This function is called periodically during operator control.
	 */
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
		updateStatus();
		
		if(DriverStation.getInstance().getMatchTime() <= 30.0){
			climbUtil.setClimb(true);
		}
	}

	/**
	 * This function is called periodically during test mode.
	 */
	@Override
	public void testPeriodic() {
	}
	
	private void updateStatus() {
		driveUtil.updateStatus();
		cubeUtil.updateStatus();
		climbUtil.updateStatus();
		autoUtil.updateStatus();
		armUtil.updateStatus();
		wristUtil.updateStatus();
		SmartDashboard.putData(autoUtil);
		SmartDashboard.putData(armUtil);
	}

	public AutoUtil getAutoUtil(){
		return autoUtil;
	}
	
	public DriveUtil getDriveUtil(){
		return driveUtil;
	}
}
