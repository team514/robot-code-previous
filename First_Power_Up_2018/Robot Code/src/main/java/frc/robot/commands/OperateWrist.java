package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.subsystems.States;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class OperateWrist extends Command {
	States.WristMode wmode;
	double calcPot, calcSpeed;
	
    public OperateWrist(States.WristMode wmode) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.wristUtil);
    	this.wmode = wmode;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	switch(Robot.states.getPlayZone()){
    		case goRestricted:
    			Robot.wristUtil.moveWristForRestricted();
    			break;
    		case inRestricted:
    			Robot.wristUtil.setWristMotor(0.0);
    		case Play:
   	    		if(!Robot.states.isRestricted()) {
   	    			Robot.wristUtil.moveWristForPlay(wmode);	
   	    		}else{
   	    			Robot.wristUtil.setWristMotor(0.0);
   	    		}
    			break;
    		default:
    			//Robot.states.setPlayZone(States.PlayZone.Play);
    			break;
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
