package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.subsystems.CubeUtil;
import frc.robot.subsystems.CubeUtil.CubeMode;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class OperateCube extends Command {
	
	CubeMode cubeMode;
	boolean done;
    
	public OperateCube(CubeUtil.CubeMode c) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.cubeUtil);
    	this.cubeMode = c;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	done = false;	
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	switch(this.cubeMode){
    	
    	case In:
    		Robot.cubeUtil.cubeIn();
    		Robot.cubeUtil.setCubeMode(this.cubeMode);
    		break;
    	case Out:
    		Robot.cubeUtil.setPiston(true);
    		Robot.cubeUtil.cubeOut();
    		Robot.cubeUtil.setCubeMode(this.cubeMode);
    		break;
    	case Stop:
    		Robot.cubeUtil.cubeStop();
    		Robot.cubeUtil.setCubeMode(this.cubeMode);
    		break;
    	case Spin:
    		Robot.cubeUtil.cubeSpin();
    		Robot.cubeUtil.setCubeMode(this.cubeMode);
    		break;
    	default:
    		Robot.cubeUtil.cubeStop();
    		Robot.cubeUtil.setCubeMode(this.cubeMode);
    	
    	}
    	
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
    	switch(this.cubeMode){
	    	case In: {
	    		if(Robot.cubeUtil.getCubeLimitSwitch()){
	    			done = true;
	    		}
	    		break;
	    	}
	    	default: {
	    		done = false;
	    	}
	    }
    	return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
