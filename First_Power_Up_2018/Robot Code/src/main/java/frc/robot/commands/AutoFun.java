package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.DriveUtil;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoFun extends Command {

    public AutoFun() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    	requires(Robot.autoUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	Robot.driveUtil.resetEncoders();
    	Robot.driveUtil.resetGyro();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	System.out.println("HELLO " + Robot.autoUtil.calcDistance() + " <= " + RobotMap.autoStopDistance);
    	if(Robot.autoUtil.calcDistance() > 5.99){
    		return;
    	}
		if(Robot.autoUtil.calcDistance() > RobotMap.autoStopDistance) {
	    	Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Camera);
		}else{
			Robot.driveUtil.stopDrive();
		}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
