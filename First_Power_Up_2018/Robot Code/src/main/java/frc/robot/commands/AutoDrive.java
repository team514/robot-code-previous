package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.AutoUtil;
import frc.robot.subsystems.DriveUtil;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoDrive extends Command {

	int loc, step;
	boolean done;

	public AutoDrive(int loc) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.autoUtil);
    	requires(Robot.driveUtil);
    	
    	this.loc = loc;
    	this.done = false;
    	this.step = 0;
    	
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	Robot.driveUtil.resetEncoders();
    	Robot.driveUtil.resetGyro();
    	
    	// Add NearSwitch reset encoders and Gyro
    	this.step = 0;
    	this.done = false;
    	
    	
    	if(Robot.autoUtil.getStartLocation() == null ||
    		Robot.autoUtil.getStartLocation() == AutoUtil.StartLocation.Auto){
    		
    		switch (loc) {
    		
    		case 1:
    			Robot.autoUtil.setStartLocation(AutoUtil.StartLocation.Left);
    		break;
    		
    		case 2:
    			Robot.autoUtil.setStartLocation(AutoUtil.StartLocation.Center);
    		break;
    		
    		case 3:
    			Robot.autoUtil.setStartLocation(AutoUtil.StartLocation.Right);
    		break;	
    			
    		}
    	}
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	switch (Robot.autoUtil.getStartLocation()) {
        
        case Auto:
        	Robot.driveUtil.stopDrive();
        	done = true;
        break;
        
        case Left:
        	performNearSwitchLeft();
        break;
        
        case Center:
        	if(Robot.autoUtil.getNearSwitch() == 'R') {
        		performCenterRight();
        	}
        			
        	if(Robot.autoUtil.getNearSwitch() == 'L') {
        		performCenterLeft();
        	}
        break;
        case Right:
        	performNearSwitchRight();
        	break;	
        }
    	Robot.autoUtil.setAutoDriveDone(this.done);        
    }

    

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
    	Robot.autoUtil.setAutoDriveDone(this.done);
        return this.done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    private void performNearSwitchLeft(){
    	if(Robot.autoUtil.getNearSwitch() == 'L'){
    		//We're on the same side as the switch
    		switch(step) {
		
    		case 0:
    			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.NearSwitch0*RobotMap.tickFoot)) {
    				Robot.driveUtil.stopDrive();
    				step++;
    			}else{
    				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    			}
    			break;
			
    		case 1:
    			if(Robot.driveUtil.getGyroAngle() >= -90) {
    				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
    			}else{
    				Robot.driveUtil.stopDrive();
    				step++;
    			}
    			break;
			
    		case 2:
    			if(Robot.driveUtil.getLeftEncoder() <= (RobotMap.NearSwitch2*RobotMap.tickFoot)) {
    				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    			}else{
    				Robot.driveUtil.stopDrive();
    				done = true;
    				step++;
    			}
    			break;
    		}
    	}else{
		//We're on the same side as the scale.  Go there instead.
		
    		if(Robot.autoUtil.getScale() == 'L') {
    			Robot.autoUtil.setAutoMode(AutoUtil.AutoMode.ScoreScale);
    			performScaleLeft();
    			
    		}else{
    			//performSwitchOppositeLeft();
    			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.Line*RobotMap.tickFoot)){
    	    		Robot.driveUtil.stopDrive();
    	    		done = true;
    	    	}else{
    	    		Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    	    	}
    		}
    	}

    }
    
    private void performNearSwitchRight(){
    	if(Robot.autoUtil.getNearSwitch() == 'R'){
    		//We're on the same side as the switch
    		switch(step) {
    		
    		case 0:
    			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.NearSwitch0*RobotMap.tickFoot)) {
    				Robot.driveUtil.stopDrive();
    				step++;
    			}else{
    				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    			}
    			break;
    			
    		case 1:
    			if(Robot.driveUtil.getGyroAngle() <= 90) {
    				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
    			}else{
    				Robot.driveUtil.stopDrive();
    				step++;
    			}
    			break;
    			
    		case 2:
    			if(Robot.driveUtil.getLeftEncoder() <= (RobotMap.NearSwitch2*RobotMap.tickFoot)) {
    				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    			}else{
    				Robot.driveUtil.stopDrive();
    				done = true;
    				step++;
    			}
    			break;
    		}
    	}else{
    		//We're on the opposite side of the switch.  Lets go for the Scale instead
    		if(Robot.autoUtil.getScale() == 'R') {
    			Robot.autoUtil.setAutoMode(AutoUtil.AutoMode.ScoreScale);
    			performScaleRight();
    		}else{
    			//performSwitchOppositeRight();
    			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.Line*RobotMap.tickFoot)){
    	    		Robot.driveUtil.stopDrive();
    	    		done = true;
    	    	}else{
    	    		Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    	    	}
    		}
    	}
    }
    
    
    private void performCenterLeft(){
    	switch(step) {
		case 0:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (RobotMap.Center0*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 1:
			if(Robot.driveUtil.getGyroAngle() >= -90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 2:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) < (RobotMap.CenterL2*RobotMap.tickFoot)) {
    				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 3:
			if(Robot.driveUtil.getGyroAngle() >= -90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 4:
			/*
			Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Camera);
			if(Robot.autoUtil.calcDistance() <= RobotMap.autoStopDistance) {
				Robot.driveUtil.stopDrive();
				done = true;
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done = true;
			}
			*/
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (RobotMap.Center4*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done=true;
			}
			
			break;
		}
	}
    
    private void performCenterRight(){
    	switch(step) {
		case 0:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (RobotMap.Center0*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
 		case 1:
			if(Robot.driveUtil.getGyroAngle() <= 90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			
			break;
		case 2:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) < (RobotMap.CenterR2*RobotMap.tickFoot)) {
    				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 3:
			if(Robot.driveUtil.getGyroAngle() <= 90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 4:
			
			/*Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Camera);
			if(Robot.autoUtil.calcDistance() <= RobotMap.autoStopDistance) {
				Robot.driveUtil.stopDrive();
				done = true;
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done = true;
			}
			*/
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (RobotMap.Center4*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done=true;
			}
			
			break;
		}

    }
    
    private void performScaleLeft(){
    	switch(step) {
		
		case 0:
			Robot.autoUtil.setAutoMode(AutoUtil.AutoMode.ScoreScale);
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.Scale0*RobotMap.tickFoot)) {
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			break;
			
		case 1:
			if(Robot.driveUtil.getGyroAngle() >= -90) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
			
		case 2:
			if(Robot.driveUtil.getLeftEncoder() <= (RobotMap.Scale2*RobotMap.tickFoot)) {
				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				done = true;
				step++;
			}
			break;	
		}
    }
    
    private void performScaleRight(){
    	switch(step) {
		
		case 0:
			Robot.autoUtil.setAutoMode(AutoUtil.AutoMode.ScoreScale);
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.Scale0*RobotMap.tickFoot)) {
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			break;
			
		case 1:
			if(Robot.driveUtil.getGyroAngle() <= 90) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
			
		case 2:
			if(Robot.driveUtil.getLeftEncoder() <= (RobotMap.Scale2*RobotMap.tickFoot)) {
				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				done = true;
				step++;
			}
			break;	
		}
    }
    public void performSwitchOppositeLeft(){
    	switch(step){
		
		case 0:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite0*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			break;
		
		case 1:
			if(Robot.driveUtil.getGyroAngle() <= 90){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}
			
			break;
			
		case 2:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite2*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			
			break;
			
		case 3:
			if(Robot.driveUtil.getGyroAngle() <= 90){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}
			
			break;
			
		case 4:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite4*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			break;
			
		case 5:
			if(Robot.driveUtil.getGyroAngle() >= -90){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}
			
			break;
			
		case 6:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite6*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				done = true;
				step++;
			}else{
				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			
			break;
		}
    }
    
    public void performSwitchOppositeRight(){
    	switch(step){
		
		case 0:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite0*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			break;
		
		case 1:
			if(Robot.driveUtil.getGyroAngle() >= -90){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}
			
			break;
			
		case 2:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite2*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			
			break;
			
		case 3:
			if(Robot.driveUtil.getGyroAngle() >= -90){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}
			
			break;
			
		case 4:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite4*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			break;
			
		case 5:
			if(Robot.driveUtil.getGyroAngle() <= 90){
				Robot.driveUtil.stopDrive();
				step++;
			}else{
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}
			
			break;
			
		case 6:
			if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.SwitchOpposite6*RobotMap.tickFoot)){
				Robot.driveUtil.stopDrive();
				done = true;
				step++;
			}else{
				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}
			
			break;
		}

    }
}
