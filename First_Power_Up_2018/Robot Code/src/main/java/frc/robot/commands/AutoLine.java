package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.AutoUtil;
import frc.robot.subsystems.DriveUtil;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoLine extends Command {
	
		int loc, step;
		boolean done;
    
	public AutoLine(int loc) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.autoUtil);
    	requires(Robot.driveUtil);
    	
    	this.loc = loc;
    	this.done = false;
    	this.step = 0;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	
    	Robot.driveUtil.resetEncoders();
    	Robot.driveUtil.resetGyro();
    	
    	this.step = 0;
    	
    	if(Robot.autoUtil.getStartLocation() == null ||
    		Robot.autoUtil.getStartLocation() == AutoUtil.StartLocation.Auto){
    		
    		switch (loc) {
    		
    		case 1:
    			Robot.autoUtil.setStartLocation(AutoUtil.StartLocation.Left);
    		break;
    		
    		case 2:
    			Robot.autoUtil.setStartLocation(AutoUtil.StartLocation.Center);
    		break;
    		
    		case 3:
    			Robot.autoUtil.setStartLocation(AutoUtil.StartLocation.Right);
    		break;	
    			
    		}
    	
    	}
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    
    switch (Robot.autoUtil.getStartLocation()) {
    
    case Auto:
    	Robot.driveUtil.stopDrive();
    	done = true;
    break;
    
    case Left:
    	if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.Line*RobotMap.tickFoot)){
    		Robot.driveUtil.stopDrive();
    		done = true;
    	}else{
    		Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    	}
    break;
    
    case Center:
    	if(Robot.autoUtil.getNearSwitch() == 'R') {
    		performCenterRight();
    	}
    			
    	if(Robot.autoUtil.getNearSwitch() == 'L') {
    		performCenterLeft();
    	}
    	break;
    case Right:
    	if(Robot.driveUtil.getLeftEncoder() >= (RobotMap.Line*RobotMap.tickFoot)){
    		Robot.driveUtil.stopDrive();
    		done = true;
    	}else{
    		Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
    	}
    	break;	
    }
    
    Robot.autoUtil.setAutoDefaultDone(this.done);
    
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.done;   
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    private void performCenterRight(){
		switch(step) {
		case 0:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (RobotMap.Center0*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
 		case 1:
			if(Robot.driveUtil.getGyroAngle() <= 90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			
			break;
		case 2:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) < (RobotMap.CenterR2*RobotMap.tickFoot)) {
    				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 3:
			if(Robot.driveUtil.getGyroAngle() <= 90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.Clockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 4:			
			if(Robot.autoUtil.calcDistance() >= RobotMap.autoStopDistance) {
				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Camera);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done = true;
			}
			/*
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (5.0*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done=true;
			}
			*/
			break;
		}
    }
    
    private void performCenterLeft(){
		switch(step) {
		case 0:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (RobotMap.Center0*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 1:
			if(Robot.driveUtil.getGyroAngle() >= -90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 2:
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) < (RobotMap.CenterL2*RobotMap.tickFoot)) {
    				Robot.driveUtil.autoStraight(RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 3:
			if(Robot.driveUtil.getGyroAngle() >= -90.0) {
				Robot.driveUtil.autoTurn(RobotMap.autoTurnSpeed, DriveUtil.Turn.CounterClockwise);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
			}
			break;
		case 4:		
			if(Robot.autoUtil.calcDistance() >= RobotMap.autoStopDistance) {
				Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Camera);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done = true;
			}
			/*
			if(Math.abs(Robot.driveUtil.getLeftEncoder()) <= (5.0*RobotMap.tickFoot)) {
    			Robot.driveUtil.autoStraight(-RobotMap.autoDriveSpeed, DriveUtil.Controller.Gyro);
			}else{
				Robot.driveUtil.stopDrive();
				step++;
				done=true;
			}
			*/
			break;
		}
    	
    }
}
