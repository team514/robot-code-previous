
package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.States;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoScore extends Command {
	
	boolean done;
	int step;
	
    public AutoScore() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.armUtil);
    	requires(Robot.wristUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	this.done = false;
    	this.step = 0;
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	if(Robot.autoUtil.bypassScore()){
    		this.done = true;
    	}else{
        	switch(Robot.autoUtil.getAutoMode()){
        	case ScoreSwitch:
        		performScoreSwitch();
        		break;
        	case ScoreScale:
        		performScoreScale();
        		break;
        		default:
        			break;
        	}	
    	}
    	Robot.autoUtil.setAutoScoreDone(this.done);
    }
    
    private void performScoreSwitch(){
    	switch(step){
    	case 0:
    		//Robot.wristUtil.setWristMotor(0.0);
    		//Robot.states.setWristMode(States.WristMode.Stop);
    		moveWristIntoSafePosition();
    		//moveArmIntoAutoPosition();
    		break;
    	case 1:
    		moveArmIntoAutoPosition();
    		break;
    	case 2:
    		moveWristIntoAutoPosition();
    		break;
    	case 3:
    		Robot.cubeUtil.cubeOut();
    		Timer.delay(1.0);
    		this.step++;
    		break;
    		default:
    			Robot.cubeUtil.cubeStop();
    			break;
    	}
    }
    
    private void performScoreScale(){
    	switch(step){
    	case 0:
    		//Robot.wristUtil.setWristMotor(0.0);
    		//Robot.states.setWristMode(States.WristMode.Stop);
    		moveWristIntoSafePosition();
    		break;
    	case 1:
    		moveArmIntoAutoPosition();    		
    		break;
    	case 2:
    		moveWristIntoAutoPosition();
    		break;
    	case 3:
    		Robot.cubeUtil.cubeOut();
    		//Timer.delay(1.0);
    		this.step++;
    		break;
    		default:
    			Robot.cubeUtil.cubeStop();
    			break;
    	}
    }
    
    private void moveWristIntoSafePosition(){
		if((Robot.wristUtil.getWristPot() >= RobotMap.wristLowerConstraint) &&
	    		(Robot.wristUtil.getWristPot() <= RobotMap.wristUpperConstraint)){
	    		Robot.wristUtil.setWristMotor(0.0);
	    		Robot.states.setWristMode(States.WristMode.Stop);
	    		this.step++;
	    	}else{
	    		if(Robot.wristUtil.getWristPot() > RobotMap.wristUpperConstraint){
	    			Robot.wristUtil.setWristMotor(-RobotMap.wristSpeed);
	    			Robot.states.setWristMode(States.WristMode.Down);
	    		} else if (Robot.wristUtil.getWristPot() < RobotMap.wristLowerConstraint){
	    			Robot.wristUtil.setWristMotor(RobotMap.wristSpeed);
	    			Robot.states.setWristMode(States.WristMode.Up);
	    		}
	    	}    	    	
    }
    
    private void moveArmIntoAutoPosition(){
		if((Robot.armUtil.getArmPot() > RobotMap.armAutoLow) &&
    		(Robot.armUtil.getArmPot() < RobotMap.armAutoHigh)){
    		Robot.armUtil.setArmMotor(0.0);
    		Robot.states.setArmMode(States.ArmMode.Stop);
    		Robot.armUtil.brakeOn();
    		this.step++;
    	}else{
    		if(Robot.armUtil.getArmPot() < RobotMap.armAutoLow){
           		Robot.armUtil.setArmMotor(1.3* RobotMap.armSpeed);
           		Robot.states.setArmMode(States.ArmMode.Up);
           		Robot.armUtil.brakeOff();
    		} else if (Robot.armUtil.getArmPot() > RobotMap.armAutoHigh){
           		Robot.armUtil.setArmMotor(-RobotMap.armSpeed);
           		Robot.states.setArmMode(States.ArmMode.Down);
           		Robot.armUtil.brakeOff();
    		}
    	}    		    	
    }

    private void moveWristIntoAutoPosition(){
    	switch(Robot.autoUtil.getAutoMode()){
    	case ScoreScale:
    		if((Robot.wristUtil.getWristPot() >= RobotMap.wristAutoScaleLow) &&
    	    		(Robot.wristUtil.getWristPot() <= RobotMap.wristAutoScaleHigh)){
    	    		Robot.wristUtil.setWristMotor(0.0);
    	    		Robot.states.setWristMode(States.WristMode.Stop);
    	    		this.step++;
    	    	}else{
    	    		if(Robot.wristUtil.getWristPot() > RobotMap.wristAutoScaleHigh){
    	    			Robot.wristUtil.setWristMotor(-RobotMap.wristSpeed);
    	    			Robot.states.setWristMode(States.WristMode.Down);
    	    		} else if (Robot.wristUtil.getWristPot() < RobotMap.wristAutoScaleLow){
    	    			Robot.wristUtil.setWristMotor(RobotMap.wristSpeed);
    	    			Robot.states.setWristMode(States.WristMode.Up);
    	    		}
    	    	}    	
    		break;
    	case ScoreSwitch:
    		if((Robot.wristUtil.getWristPot() >= RobotMap.wristAutoSwitchLow) &&
    	    		(Robot.wristUtil.getWristPot() <= RobotMap.wristAutoSwitchHigh)){
    	    		Robot.wristUtil.setWristMotor(0.0);
    	    		Robot.states.setWristMode(States.WristMode.Stop);
    	    		this.step++;
    	    	}else{
    	    		if(Robot.wristUtil.getWristPot() > RobotMap.wristAutoSwitchHigh){
    	    			Robot.wristUtil.setWristMotor(-RobotMap.wristSpeed);
    	    			Robot.states.setWristMode(States.WristMode.Down);
    	    		} else if (Robot.wristUtil.getWristPot() < RobotMap.wristAutoSwitchLow){
    	    			Robot.wristUtil.setWristMotor(RobotMap.wristSpeed);
    	    			Robot.states.setWristMode(States.WristMode.Up);
    	    		}
    	    	}    	    		
    		break;
    		default:
    			break;
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
