package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.ClimbUtil.climbMode;
import frc.robot.subsystems.States;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class OperateClimb extends Command {
	
	climbMode cm;
	int step;

    public OperateClimb(ClimbUtil.climbMode cm) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.climbUtil);
    	this.cm = cm;
    }

    // Called just before this Command runs the first time
    protected void initialize() {    	
    	step = 0;
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	//if(Robot.climbUtil.canClimb()){
    	if(true) {
    		switch(this.cm){
    	
    		case Stop:
    				Robot.climbUtil.setClimbSpeed(0.0);
    				Robot.climbUtil.setClimbMode(climbMode.Stop);
    			break;
    		
    		case ClimbUp:
    			//Robot.climbUtil.setClimbSpeed(1.0);
    			//performClimbSequence();
    			Robot.climbUtil.setClimbSpeed(1.0);
   				Robot.climbUtil.setClimbMode(climbMode.ClimbUp);
    			break;
    		
    		case ClimbDown:
    			Robot.climbUtil.setClimbSpeed(-1.0);
   				Robot.climbUtil.setClimbMode(climbMode.ClimbDown);
    			break;
    			
    		
    		default:
    			Robot.climbUtil.setClimbSpeed(0.0);
    			Robot.climbUtil.setClimbMode(climbMode.Stop);
    		}
    	}
    }
    
    public void performClimbSequence(){
    	// Validate the assumptions:
    	//      1.  The operator will move the arm into position.
    	//      2.  The operator will move the wrist into position.
    	//      3.  The operator will ensure the hook is attached.
    	//      4.  The wrist will be in an UNSAFE position to fully return the arm.
    	//      5.  The wrist will have to be in the SAFE state before we enter the Restricted Zone
    	//
    	switch(step){
    	case 0:
    		//Clear the Arm and Wrist from the tower.
			if(Robot.armUtil.getArmPot() > RobotMap.armUpperConstraint + 100.0){
				Robot.armUtil.moveArmInRestrictedZone(States.ArmMode.Down);
			}else{
				Robot.armUtil.moveArmInRestrictedZone(States.ArmMode.Stop);
				step++;
			}
    		break;
    	case 1:
    		//Stop Arm to put Wrist in Safe Position.
			if(Robot.states.getWristMode() == States.WristMode.Safe){
				Robot.wristUtil.setWristMotor(0.0);
				Robot.states.setWristMode(States.WristMode.Safe);
				step++;
			}else{
				Robot.wristUtil.moveWristForRestricted();
			}
    		break;
    	case 2:
    		//Run the Arm below lower constraint and start the climb
    		if(Robot.armUtil.getArmPot() <= RobotMap.armLowerConstraint){
				Robot.armUtil.moveArmInRestrictedZone(States.ArmMode.Stop);
    		}else{
				Robot.armUtil.moveArmInRestrictedZone(States.ArmMode.Down);
    		}
			Robot.climbUtil.setClimbSpeed(RobotMap.climbUpSpeed);
			Robot.climbUtil.setClimbMode(climbMode.ClimbUp);    		
    		break;
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
