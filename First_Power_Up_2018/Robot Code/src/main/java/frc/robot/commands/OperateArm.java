package frc.robot.commands;


import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.States;
import frc.robot.subsystems.States.PlayZone;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class OperateArm extends Command {
	States.ArmMode am;
	
    public OperateArm(States.ArmMode am) {
    	requires(Robot.armUtil);
    	this.am = am;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	setPlayZone();
    	switch(Robot.states.getPlayZone()){
    		case goRestricted:
    			//Check if wrist is in a position to allow arm movement
    			if(Robot.states.getWristMode() == States.WristMode.Safe){
    				Robot.states.setWristPosition(States.WristPosition.Safe);
    				Robot.armUtil.moveArmInRestrictedZone(am);
    			}else{
    			//If not, wait for the wrist to be in a position to allow arm movement.
    				Robot.armUtil.setArmMotor(0.0);
    				Robot.states.setArmMode(States.ArmMode.Stop);
    			}
    			break;
    		case inRestricted:
    			Robot.armUtil.moveArmInRestrictedZone(am);
    			break;
    		case Play:
   	    		Robot.armUtil.moveArmForPlay(am);
    			break;
    		default:
    			//Robot.states.setPlayZone(States.PlayZone.Play);
    			break;
    	}
    }

    private void setPlayZone(){
    	
    	if(Robot.oi.getdPad() == 0){

    		if(Robot.states.getPlayZone() == PlayZone.Play){
        		Robot.states.setPlayZone(States.PlayZone.goRestricted);
        		Robot.states.setWristPosition(States.WristPosition.Up);
    		}
    	}
    	
    	/*
    	if(Robot.oi.getdPad() == 90){
    		if(Robot.states.getPlayZone() == States.PlayZone.goRestricted){
        		Robot.states.setWristPosition(States.WristPosition.Up);
    			Robot.states.setPlayZone(States.PlayZone.Play);
    		}
    	}
    	*/
    	
    	/*if(Robot.oi.getdPad() == 180){
    		// Put code in to move into the Restricted Zone with the wrist down
    		Robot.states.setPlayZone(States.PlayZone.goRestricted);
    		Robot.states.setWristPosition(States.WristPosition.Down);
    	}*/
    	
    	if((Robot.states.getPlayZone() == States.PlayZone.inRestricted) &&
    	   (Robot.armUtil.getArmPot() >= RobotMap.armUpperConstraint)){
    		Robot.states.setPlayZone(States.PlayZone.Play);
    	}
    	if((Robot.states.getPlayZone() == States.PlayZone.inRestricted) &&
    		(Robot.armUtil.getArmPot() <= RobotMap.armLowerConstraint)){
    		Robot.states.setPlayZone(States.PlayZone.Play);
    	}
    	if((Robot.states.getPlayZone() == States.PlayZone.goRestricted) &&
    			((Robot.armUtil.getArmPot() > RobotMap.armLowerConstraint) &&
    		(Robot.armUtil.getArmPot() < RobotMap.armUpperConstraint))){
    		Robot.states.setPlayZone(States.PlayZone.inRestricted);
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
