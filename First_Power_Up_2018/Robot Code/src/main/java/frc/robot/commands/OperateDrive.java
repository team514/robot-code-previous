package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.subsystems.DriveUtil;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class OperateDrive extends Command {

	public DriveUtil.DriveMode driveMode;
	
    public OperateDrive(DriveUtil.DriveMode d) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	
    	requires(Robot.driveUtil);
    	this.driveMode = d;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	
    	switch(this.driveMode){
    	
    	case Tank:
        	Robot.driveUtil.driveTank(Robot.oi.getLeftY(),Robot.oi.getRightY()); 
        	Robot.driveUtil.setModeState(DriveUtil.DriveMode.Tank);
    		break;
    	
    	case Arcade:
    		Robot.driveUtil.driveArcade(Robot.oi.getRightX(), Robot.oi.getRightY());
    		Robot.driveUtil.setModeState(DriveUtil.DriveMode.Arcade);
    		break;
    		
    	default:
    		Robot.driveUtil.driveTank(Robot.oi.getLeftY(),Robot.oi.getRightY());
    		Robot.driveUtil.setModeState(DriveUtil.DriveMode.Tank);
    		break;    		
    	}
    	
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
