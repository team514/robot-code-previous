/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.commands.OperateArm;
import frc.robot.commands.OperateClimb;
import frc.robot.commands.OperateCube;
import frc.robot.commands.OperateDrive;
import frc.robot.commands.OperateWrist;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.CubeUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.States;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
	
	Joystick leftstick, rightstick, controller;
	JoystickButton tank, arcade, 
					cubeIn, cubeOut, cubeSpin, 
					climbUp, climbDown, 
					armUp, armDown, 
					wristUp, wristDown,
					toggleGrip,
					deployHook,
					sophia;
	
	public OI(){

		rightstick = new Joystick(0);
		leftstick = new Joystick(1);
		controller = new Joystick(2);
		
		//Instantiate Buttons
		tank = new JoystickButton(rightstick, RobotMap.DriveTank);
		arcade = new JoystickButton(rightstick, RobotMap.DriveArcade);
	
		cubeIn = new JoystickButton(controller, RobotMap.kXButtonNum);
		cubeOut = new JoystickButton(controller, RobotMap.kBButtonNum);
		cubeSpin = new JoystickButton(controller, RobotMap.kAButtonNum);

		climbUp = new JoystickButton(controller, RobotMap.kStartButtonNum);
		climbDown = new JoystickButton(controller, RobotMap.kBackButtonNum);

		wristUp = new JoystickButton(controller, RobotMap.kLeftBumperNum);
		wristDown = new JoystickButton(controller, RobotMap.kLeftTriggerNum);

		armUp = new JoystickButton(controller, RobotMap.kRightBumperNum);
		armDown = new JoystickButton(controller, RobotMap.kRightTriggerNum);

		deployHook = new JoystickButton(controller, RobotMap.kLeftStickButtonNum);
		//toggleGrip = new JoystickButton(controller, RobotMap.kStartButtonNum);
		
		sophia = new JoystickButton(rightstick, RobotMap.SophiaButton);
		
		//Execute Commands
		tank.whenPressed(new OperateDrive(DriveUtil.DriveMode.Tank));
		arcade.whenPressed(new OperateDrive(DriveUtil.DriveMode.Arcade));

		armUp.whileHeld(new OperateArm(States.ArmMode.Up));
		armDown.whileHeld(new OperateArm(States.ArmMode.Down));

		wristDown.whileHeld(new OperateWrist(States.WristMode.Down));
		wristUp.whileHeld(new OperateWrist(States.WristMode.Up));			

		deployHook.whenPressed(new OperateWrist(States.WristMode.Hook));
		
		sophia.whenPressed(new OperateWrist(States.WristMode.Sophia));
		
		cubeIn.whileHeld(new OperateCube(CubeUtil.CubeMode.In));
		cubeOut.whileHeld(new OperateCube(CubeUtil.CubeMode.Out));
		cubeSpin.whileHeld(new OperateCube(CubeUtil.CubeMode.Spin));

		climbUp.whileHeld(new OperateClimb(ClimbUtil.climbMode.ClimbUp));
		climbDown.whileHeld(new OperateClimb(ClimbUtil.climbMode.ClimbDown));
		
		// I don't think we need this toggle any more but can leave it just in case.
		//toggleGrip.whenPressed(new OperateGrip());
	}
	
	public double getRightY(){
		
		return rightstick.getY();
		
	}
	
	public double getRightX(){
		return rightstick.getX();
	}
	
	public double getLeftY(){
	
		return leftstick.getY();
	}
	
	public int getdPad(){
		return controller.getPOV();
	}
}
