package frc.robot.subsystems;

import frc.robot.Robot;
import frc.robot.RobotMap;

public class States {
	double speed, wpot;
	static PlayZone pz = PlayZone.Play;
	static ArmMode amode = ArmMode.Stop;
	static WristMode wmode = WristMode.Stop;
	static WristPosition wpos = WristPosition.Up;
	boolean restricted, safe;
	
	public States(){
		restricted = false;
		safe = false;
	}
	
    public enum ArmMode {
    	Up,
    	Down, 
    	Stop;
    }
    
    public enum WristMode {
    	Up,
    	Down,
    	Safe,
    	Stop,
    	Hook,
    	Sophia;
    }
    
    public enum WristPosition {
    	Up,
    	Down,
    	Safe;
    }
    
    public enum PlayZone {
    	goRestricted,
    	inRestricted,
    	Play;
    }
    
    public void setWristPosition(WristPosition wp){
    	wpos = wp;
    }
    
    public WristPosition getWristPosition(){
    	return wpos;
    }
    
    public String getWristPosString(){
    	return wpos.toString();
    }

    public void setPlayZone(PlayZone c){
    	pz = c;
    }
    
    public PlayZone getPlayZone(){
    	return pz;
    }
    
    public void setArmMode(ArmMode aMode){
    	amode = aMode;
    }
    
    public void setWristMode(WristMode wMode){
    	wmode = wMode;
    }
    
    public ArmMode getArmMode(){
    	return amode;
    }
    
    public WristMode getWristMode(){
    	return wmode;
    }
    public String getArmString() {
    	return amode.toString();
    }
    
    public String getWristString() {
    	return wmode.toString();
    }
    
    public String getPlayString(){
    	return pz.toString();
    }
    
    public boolean getRestricted() {
    	if((getPlayZone() == PlayZone.goRestricted) ||
    		(getPlayZone() == PlayZone.inRestricted)){
    		this.restricted = true;
    	}
    	if(getPlayZone() == PlayZone.Play) {
    		this.restricted = false;
    	}
    	return this.restricted;
    }
    
    public boolean isRestricted() {
    	if(getPlayZone() == PlayZone.inRestricted) {
    		return true;
    
    	}else{
    		return false;
    	}
    }
    
    public boolean getSafe() {
    	if(getWristPosition() == WristPosition.Safe) {
    		this.safe = true;
    	}else{
    		this.safe = false;
    	}
    	return this.safe;
    }
    
    public double calcWristValue(){
    	wpot = ((0.0018*(Math.pow(Robot.armUtil.getArmPot(), 2))) - (0.6601*Robot.armUtil.getArmPot()) + 112.12);
    	return this.wpot;
    }
    public double getWristSpeed(double input){
    	double d = coerce2Range(input, 
				RobotMap.C2R_inputWristMin, 
				RobotMap.C2R_inputWristMax, 
				RobotMap.C2R_outputWristMin, 
				RobotMap.C2R_outputWristMax);
    	return d;
    }
    
	private double coerce2Range(double input, double imin, double imax, double omin, double omax){
        double inputMin, inputMax, inputCenter;
        double outputMin, outputMax, outputCenter;
        double scale, result;
        //double output;
        
        inputMin = imin; 
        inputMax = imax;
        
        outputMin = omin;
        outputMax = omax;
        
        /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);
            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;
            /* Constrain to the output range */
            speed = Math.max(Math.min(result, outputMax), outputMin);

       return speed;
    }
    

}
