package frc.robot.subsystems;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.OperateWrist;

import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class WristUtil extends Subsystem {
	VictorSP wristmotor;
	AnalogPotentiometer wristpot;
	
	public WristUtil() {
		wristmotor = new VictorSP(RobotMap.cubeMotorWrist);
		wristpot = new AnalogPotentiometer(RobotMap.cubeWristPot);
	}

    // Put methods for controlling this subsystem
    // here. Call these from Commands.

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new OperateWrist(States.WristMode.Stop));
    }
    
    public void setWristMotor(double speed){
    	wristmotor.set(speed);
    }
    
    public double getWristPot(){
    	//Invert the value to simplify logic coding.
    	// In the past we normally use pot.pidGet(); method.  This should be tested.
    	//      may eliminate the need to scale the value to units 100s.
    	return (1000.0 - (1000.0*wristpot.get()));
    }

    public void moveWristForRestricted(){
    	switch(Robot.states.getWristPosition()){
    	case Up:
        	if((getWristPot() >= RobotMap.wristLowerConstraint) &&
            		(getWristPot() <= RobotMap.wristUpperConstraint)){
               			Robot.states.setWristMode(States.WristMode.Safe);
               			Robot.states.setWristPosition(States.WristPosition.Safe);
               			setWristMotor(0.0);
           	}else{
           		if(getWristPot() < RobotMap.wristLowerConstraint){
           			setWristMotor(RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Up);
           		}
           		if(getWristPot() > RobotMap.wristUpperConstraint){
           			setWristMotor(-RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Down);    			
           		}
           	}    		
    		break;
    	case Down:
    		// Confirm we can use Auto Score - Scale settings for this mode.
        	if((getWristPot() >= RobotMap.wristDownLowerConstraint) &&
           		(getWristPot() <= RobotMap.wristDownUpperConstraint)){
           			Robot.states.setWristMode(States.WristMode.Safe);
           			Robot.states.setWristPosition(States.WristPosition.Safe);
           			setWristMotor(0.0);
           	}else{
           		if(getWristPot() < RobotMap.wristDownLowerConstraint){
           			setWristMotor(RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Up);
           		}
           		if(getWristPot() > RobotMap.wristDownUpperConstraint){
           			setWristMotor(-RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Down);    			
           		}
           	}    		
    		break;
    	case Safe:
    		
        	if((getWristPot() >= RobotMap.wristLowerConstraint) &&
            		(getWristPot() <= RobotMap.wristUpperConstraint)){
               			Robot.states.setWristMode(States.WristMode.Safe);
               			Robot.states.setWristPosition(States.WristPosition.Safe);
               			setWristMotor(0.0);
           	}else{
           		if(getWristPot() < RobotMap.wristLowerConstraint){
           			setWristMotor(RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Up);
           		}
           		if(getWristPot() > RobotMap.wristUpperConstraint){
           			setWristMotor(-RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Down);    			
           		}
           	} 
           	 
    		//setWristMotor(0.0);
    			break;
    	}
    }    
    
    public void deployHook() {
    	if((getWristPot() >= RobotMap.hookWristLowerLimit) &&
           		(getWristPot() <= RobotMap.hookWristUpperLimit)){
           			Robot.states.setWristMode(States.WristMode.Safe);
           			Robot.states.setWristPosition(States.WristPosition.Safe);
           			setWristMotor(0.0);
           	}else{
           		if(getWristPot() < RobotMap.hookWristLowerLimit){
           			setWristMotor(RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Up);
           		}
           		if(getWristPot() > RobotMap.hookWristUpperLimit){
           			setWristMotor(-RobotMap.wristSpeed);
           			Robot.states.setWristMode(States.WristMode.Down);    			
           		}
           	}    		
    }
    
    public void moveWristForPlay(States.WristMode wmode){
    	switch(wmode) {
    	
		case Up:
			if(getWristPot() >= RobotMap.wristUpperLimit){
				Robot.states.setWristMode(States.WristMode.Stop);
				setWristMotor(0.0);
			}else{
    			Robot.states.setWristMode(States.WristMode.Up);
    			setWristMotor(RobotMap.wristSpeed);    				
			}
			break;
		
		case Down:
			if(getWristPot() <= RobotMap.wristLowerLimit){
				Robot.states.setWristMode(States.WristMode.Stop);
				setWristMotor(0.0);
			}else{
    			Robot.states.setWristMode(States.WristMode.Down);
    			setWristMotor(-RobotMap.wristSpeed);    				
			}
			break;
		
		case Hook:
			// Add 30 second match timer limitation
			deployHook();
			break;
			
		case Stop:
			Robot.states.setWristMode(States.WristMode.Stop);
			setWristMotor(0.0);
			break;
			
		case Sophia:
			if(getWristPot() < RobotMap.sophiaWristLowerLimit){
				Robot.states.setWristMode(States.WristMode.Up);
    			setWristMotor(RobotMap.wristSpeed);
			}else{
				Robot.states.setWristMode(States.WristMode.Stop);
    			setWristMotor(0.0);
			}
			break;
			
		default:
			break;
		}    	
    }

    public void updateStatus() {
    	SmartDashboard.putNumber("Wrist Pot : ", getWristPot());
    	SmartDashboard.putString("Wrist Mode : ", Robot.states.getWristString());
    	SmartDashboard.putBoolean("Wrist Safe", Robot.states.getSafe());
    	SmartDashboard.putString("Wrist Position : ", Robot.states.getWristPosString());
    }
}

