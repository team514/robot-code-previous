package frc.robot.subsystems;

import frc.robot.RobotMap;
import frc.robot.commands.OperateClimb;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class ClimbUtil extends Subsystem {
	
	Spark climbMotors;
	climbMode cm;
	boolean climb;
	
	public ClimbUtil() {
		climbMotors = new Spark(RobotMap.climbMotors);
		
	}
	
	public void setClimb(boolean b){
		this.climb = b;
	}
	
	public boolean canClimb(){
		return this.climb;
	}
	
	public void setClimbMode(climbMode cm){
		this.cm = cm;
	}
	
	public climbMode getClimbMode(){
		return this.cm;
	}
	
	public void setClimbSpeed(double speed) {
		climbMotors.set(speed);
	}
	
	
	

    // Put methods for controlling this subsystem
    // here. Call these from Commands.

    public void initDefaultCommand() {
    	
    	setDefaultCommand(new OperateClimb(ClimbUtil.climbMode.Stop));
    	
    	
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	
    }
    
    public void updateStatus() {
    	
    }
    
    public enum climbMode {
    	
    	Stop,
    	ClimbUp,
    	ClimbDown;
    	
    }
}
