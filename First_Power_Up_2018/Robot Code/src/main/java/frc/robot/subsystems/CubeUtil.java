package frc.robot.subsystems;

import frc.robot.RobotMap;
import frc.robot.commands.OperateCube;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class CubeUtil extends Subsystem {
	
	VictorSP leftmotor, rightmotor;
	DigitalInput cubeLimit;
	CubeMode cmode;
	Solenoid ds;

	public CubeUtil() {
		leftmotor = new VictorSP(RobotMap.cubeMotorLeft);
		rightmotor = new VictorSP(RobotMap.cubeMotorRight);
		rightmotor.setInverted(true);
		cubeLimit = new DigitalInput(RobotMap.cubeLimitSwitch);
		ds = new Solenoid(RobotMap.dsChannel);
		
	}
	public boolean getCubeLimitSwitch(){
		return !cubeLimit.get();	
	}
	
	public void setPiston(boolean b){
		ds.set(b);
	}
	
	public boolean getPiston(){
		return ds.get();
	}
	
	public void cubeIn() {
		if(getCubeLimitSwitch()) {
			//Timer.delay(2.0);
			setPiston(false);
			cubeStop();
			return;
		}
		//Timer.delay(2.0);
		setPiston(true);
		leftmotor.set(-RobotMap.cubeSpeed);
		rightmotor.set(-RobotMap.cubeSpeed);
	}

	public void cubeOut() {
		leftmotor.set(RobotMap.cubeSpeed);
		rightmotor.set(RobotMap.cubeSpeed);
		//Timer.delay(2.0);
	}
	
	public void cubeStop() {
		setPiston(false);
		leftmotor.set(0.0);
		rightmotor.set(0.0);
	}
	
	public void cubeSpin(){
		setPiston(true);
		leftmotor.set(RobotMap.cubeSpeed);
		rightmotor.set(-RobotMap.cubeSpeed);
	}
	
	
    public void initDefaultCommand() {
    
    	setDefaultCommand(new OperateCube(CubeUtil.CubeMode.Stop));
    	
    }
    
    public enum CubeMode {
    	In,
    	Out,
    	Stop,
    	Spin;
    }
    
    public void setCubeMode(CubeMode cMode){
    	this.cmode = cMode;
    }
    
    public CubeMode getCubeMode(){
    	return this.cmode;
    }
    
    public void updateStatus(){
    //	SmartDashboard.putString("Cube Mode: ", this.getCubeMode().toString());
    //	SmartDashboard.putBoolean("Cube Limit Switch", getCubeLimitSwitch());
    }
    
}

