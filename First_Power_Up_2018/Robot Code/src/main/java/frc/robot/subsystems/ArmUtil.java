package frc.robot.subsystems;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.OperateArm;
import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class ArmUtil extends Subsystem {
	
	VictorSP armmotor;
	AnalogPotentiometer armpot;
	Solenoid brake;

	public ArmUtil(){
		armmotor = new VictorSP(RobotMap.cubeMotorArm);
		armpot = new AnalogPotentiometer(RobotMap.cubeArmPot);
		brake = new Solenoid(RobotMap.brakeChannel);
	}
    public void initDefaultCommand() {
      setDefaultCommand(new OperateArm(States.ArmMode.Stop));
    }
    
    public void setArmMotor(double speed){
    	armmotor.set(speed);
    }
    
    public void brakeOn(){
    	brake.set(false);
    }
    
    public void brakeOff(){
    	brake.set(true);
    }
    
    public double getArmPot(){
    	//Invert the return value to simplify logic coding.
    	// In the past we normally use pot.pidGet(); method.  This should be tested.
    	//      may eliminate the need to scale the value to units 100s.
    	return (1000.0 - (1000*armpot.get()));
    }

    public void moveArmInRestrictedZone(States.ArmMode am){
    	switch(am){
    	
    	case Up:
    		brake.set(true);
			Robot.states.setArmMode(States.ArmMode.Up);
			setArmMotor(RobotMap.armSpeed);    		
    		break;
    	
    	case Down:
    		brake.set(true);
			Robot.states.setArmMode(States.ArmMode.Down);
			setArmMotor(-RobotMap.armSpeed);    				    		
    		break;
    		
    	case Stop:
			//We drift down.  In the lower zone that is OK.  In the upper zone that is NOT OK.
			//We will set the motor to keep the arm out of the restricted zone.
			//We can put the piston for the caliper brake in this section of code.
			/*if((getArmPot() > RobotMap.armLowerConstraint) &&
			   (getArmPot() < (RobotMap.armLowerConstraint + 5.0))){
					Robot.states.setArmMode(States.ArmMode.Up);
					Robot.armUtil.setArmMotor(RobotMap.armSpeed);				
			}else{
				Robot.states.setArmMode(States.ArmMode.Stop);
				setArmMotor(0.0);				
			}*/
    		setArmMotor(0.0);
    		brake.set(false);
    		break;
    	
    	}
    }

    public void moveArmForPlay(States.ArmMode am){
		switch(am) {
    	
		case Up:
			brake.set(true);
			if((getArmPot() < RobotMap.armLowerConstraint) ||
				((getArmPot() > RobotMap.armUpperConstraint) &&
				(getArmPot() < RobotMap.armUpperLimit))){
    				Robot.states.setArmMode(States.ArmMode.Up);
    				setArmMotor(RobotMap.armSpeed);
			}else{
				Robot.states.setArmMode(States.ArmMode.Stop);
				setArmMotor(0.0);
			}
			break;
		
		case Down:
			brake.set(true);
			if((getArmPot() > RobotMap.armUpperConstraint) ||
				((getArmPot() < RobotMap.armLowerConstraint) &&
				(getArmPot() > RobotMap.armLowerLimit))){
    				Robot.states.setArmMode(States.ArmMode.Down);
    				setArmMotor(-RobotMap.armSpeed);    				
			}else{
				Robot.states.setArmMode(States.ArmMode.Stop);
				setArmMotor(0.0);    				
			}
			break;
		
		case Stop:
			//We drift down.  In the lower zone that is OK.  In the upper zone that is NOT OK.
			//We will set the motor to keep the arm out of the restricted zone.
			//We can put the piston for the caliper break in this section of code.
			/*if((getArmPot() > RobotMap.armUpperConstraint) &&
			   (getArmPot() < (RobotMap.armUpperConstraint + 5.0))){
					Robot.states.setArmMode(States.ArmMode.Up);
					Robot.armUtil.setArmMotor(RobotMap.armSpeed);				
			}else{
				Robot.states.setArmMode(States.ArmMode.Stop);
				setArmMotor(0.0);				
			}*/
			setArmMotor(0.0);
			brake.set(false);
			break;
		}
    }
    
    public void updateStatus(){
    	SmartDashboard.putNumber("Arm Pot : ", getArmPot());
    	SmartDashboard.putString("Arm Mode : ", Robot.states.getArmString());
    	SmartDashboard.putString("Arm Play Zone : ", Robot.states.getPlayString());
    	SmartDashboard.putBoolean("Restricted", Robot.states.getRestricted());
    }
}

