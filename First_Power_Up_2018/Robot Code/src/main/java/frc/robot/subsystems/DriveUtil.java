package frc.robot.subsystems;

import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.OperateDrive;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


/**
 *
 */
public class DriveUtil extends Subsystem {
	
	WPI_TalonSRX leftmaster, leftslave, rightmaster, rightslave;
	
	AnalogInput sonar;
	Encoder leftEncoder, rightEncoder;
	ADXRS450_Gyro gyro;
	
	double d_left, d_right, adjustment, speed;
		
	DriveMode modestate;
	
	
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	public DriveUtil() {
		
		gyro = new ADXRS450_Gyro(Port.kOnboardCS0);
		leftmaster = new WPI_TalonSRX(RobotMap.leftDriveMaster);
		leftslave = new WPI_TalonSRX(RobotMap.leftDriveSlave);
		rightmaster = new WPI_TalonSRX(RobotMap.rightDriveMaster);
		rightslave = new WPI_TalonSRX(RobotMap.rightDriveSlave);
		
		
		leftslave.follow(leftmaster);
		rightslave.follow(rightmaster);
		 
		leftmaster.setNeutralMode(RobotMap.motorNeutralMode);
		leftslave.setNeutralMode(RobotMap.motorNeutralMode);
		rightmaster.setNeutralMode(RobotMap.motorNeutralMode);
		rightslave.setNeutralMode(RobotMap.motorNeutralMode);

		leftEncoder = new Encoder(RobotMap.leftEncoderForward, RobotMap.leftEncoderReverse);
		rightEncoder = new Encoder(RobotMap.rightEncoderForward, RobotMap.rightEncoderReverse);
		
        //calibrateGyro();
		resetGyro();
	}
	

	public void resetGyro(){
	    	gyro.reset();
    }
	    
	public void calibrateGyro(){
	    	gyro.calibrate();
	    }
	    
	public double getGyroAngle(){
	    	return gyro.getAngle();
	    	//return 0;
	    }
		
	private void setDriveValues(double left, double right) {
		this.d_left = left;
		this.d_right = right;
	}
	
	private double getDriveRight() {
		return this.d_right;
	}
	
	private double getDriveLeft() {
		return this.d_left;
	}
	
	public void setModeState(DriveMode x) {
		this.modestate = x;
	}
	
	private DriveMode getModeState() {
		return this.modestate;
	}
	
	private String getModeString(){
		return getModeState().toString();
	}
	
	public void resetEncoders() {
		leftEncoder.reset();
		rightEncoder.reset();
	}
	
	public double getLeftEncoder() {
		double d;
		d = 0.0;
		d =(leftEncoder.get());
		
		return Math.abs(d);
	}
	
	public double getRightEncoder() {
		return Math.abs(rightEncoder.get());
	}
	
	public void driveTank(double left, double right) {
		
		left = squareInputs(left);
		right = squareInputs(right);
		leftmaster.set(ControlMode.PercentOutput, -left);
		rightmaster.set(ControlMode.PercentOutput, right);
		setDriveValues(left, right);
	}
	
	public void driveArcade(double xAxis, double yAxis){
		double left, right;
		left = yAxis - xAxis;
		right = yAxis + xAxis;
		driveTank(left, right);
	}
	
	private double squareInputs(double d){
		boolean negative = false;
		
		if(d < 0){
			negative = true;
		}
		
		d = Math.pow(d, 2);
		
		if(negative){
			d = -d;
		}
		
		return d;
	}
	
	public static enum DriveMode{
		
		Tank,
		Arcade,
		Auto;
	}
	public static enum Controller{
		
		Gyro,
		Camera;
	}
	
	public static enum Turn{
		
		Clockwise,
		CounterClockwise;
	}
	
	public void stopDrive(){
		driveTank(0.0, 0.0);
		resetEncoders();
		resetGyro();
	}
	
	public void autoStraight(double speed, Controller c){
		switch(c){
		case Gyro:
			driveAuto(-speed,
					Robot.driveUtil.getGyroAngle(),
					RobotMap.C2R_gyroInputMin,
					RobotMap.C2R_gyroInputMax,
					c);		
						
			break;
			
		case Camera:
			driveAuto(-speed, 
					Robot.autoUtil.getHorizontalOffset(), 
					RobotMap.C2R_inputMin, 
					RobotMap.C2R_inputMax,
					c);	
			break;
		}
		
	}
	
	public void autoTurn(double speed, Turn t){
		//System.out.println("TEST " + t.name());
		switch(t){
		case Clockwise:
			driveTank(-speed, speed);
			break;
			
		case CounterClockwise:
			driveTank(speed, -speed);
			break;
		}
		
	}
	public void updateStatus() {
		SmartDashboard.putNumber("Left Drive Motor Value: ", getDriveLeft());
		SmartDashboard.putNumber("Right Drive Motor Value: ", getDriveRight());
		SmartDashboard.putString("ModeState: ", getModeString());
	//	SmartDashboard.putNumber("Gyro Value: ", getGyroAngle());
	//	SmartDashboard.putNumber("Left Encoder: ", getLeftEncoder());
	//	SmartDashboard.putNumber("Right Encoder: ", getRightEncoder());
	//	SmartDashboard.putNumber("Adjustment Value: ", this.adjustment);
	}
	
	public void driveAuto(double driveSpeed, double setPoint, double min, double max, Controller c){
		double adj = coerce2Range(setPoint, min, max);
		//left = left + adj;
		//right = right + -adj;
		switch(c){
		
		case Gyro:
			this.adjustment = -adj;	
			break;
			
		case Camera:
			this.adjustment = adj;
			break;
		}
		
		d_right = driveSpeed + this.adjustment;
		d_left = driveSpeed - this.adjustment;
		driveTank(d_left, d_right);
	}
	
	public double coerce2Range(double input, double min, double max){
        double inputMin, inputMax, inputCenter;
        double outputMin, outputMax, outputCenter;
        double scale, result;
        //double output;
        
        inputMin = min; 
        inputMax = max;
        
        outputMin = RobotMap.C2R_outputMin;
        outputMax = RobotMap.C2R_outputMax;
        
        /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);
            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;
            /* Constrain to the output range */
            speed = Math.max(Math.min(result, outputMax), outputMin);

       return speed;
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new OperateDrive(DriveUtil.DriveMode.Arcade));
    }
}

