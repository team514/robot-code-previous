package frc.robot.subsystems;

import frc.robot.RobotMap;
import frc.robot.commands.CloseFloor;
import frc.robot.commands.ResetBasket;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class FuelUtil extends Subsystem {
	
	Solenoid floorControl, basketLift;
	DigitalInput atTop;
	
	public FuelUtil(){
		floorControl = new Solenoid(RobotMap.floorControl);
		basketLift = new Solenoid(RobotMap.basketLift);
		atTop = new DigitalInput(RobotMap.basketTopSwitch);
	}

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	
	public void openFloor(){
		floorControl.set(true);
		
	}
	
	public void closeFloor(){
		floorControl.set(false);
		
	}
	
	public boolean getAtTop(){
		return atTop.get();
	}
	
	public void rasieBasket(){
		basketLift.set(true);
	}
	
	public void dropBasket(){
		basketLift.set(false);
	}

    public void initDefaultCommand() {
    	setDefaultCommand(new ResetBasket());
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new CloseFloor());
    }
    
    public void updateStatus(){
    	SmartDashboard.putBoolean("Floor Up/Down: ", floorControl.get());
    	SmartDashboard.putBoolean("Basket Open/Closed: ", basketLift.get());
    	SmartDashboard.putBoolean("Basket Top Switch : ", getAtTop());
    }
}

