package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoFuel extends Command {
	boolean done, side, raisedBasket, raisedFloor;
	int step = 0;
	int trials;
	long time;

    public AutoFuel(boolean side) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    	requires(Robot.fuelUtil);
    	this.side = side;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	reset();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	performAuto();
    }
    
    private void reset(){
		Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
		Robot.driveUtil.resetGyro();
		Robot.driveUtil.resetEncoders();
    }
    
    private void performAuto(){
    	System.out.println(this.step);
    	switch(this.step){
    		case 0:
    			//Drive robot distance from alliance wall
    			if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoFuelDistance){
    				reset();
    				step++;
    				Timer.delay(RobotMap.wait);
    			}else{
    				Robot.driveUtil.driveAuto(RobotMap.autoY);
    			}
    			break;
    		case 1:
    			//Rotate to face boiler
    			if(Math.abs(Robot.driveUtil.getGyro()) >= RobotMap.autoFuelTurn){
    				Timer.delay(0.5);
    				reset();
    				step++;
    				Timer.delay(1.0);
    			}else{
    				if(!this.side){
    					Robot.driveUtil.driveMecanum(0.0, 0.0, RobotMap.autoZ, true);
    				}else{
    					Robot.driveUtil.driveMecanum(0.0, 0.0, -RobotMap.autoZ, true);
    				}
    			}
    			break;
    		case 2:
    			//Drive to boiler
    			if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoFuelDistance2){
    				reset();
    				Timer.delay(0.2);
    				Robot.fuelUtil.rasieBasket();
    				step++;
    			}else{
    				Robot.driveUtil.driveAuto(-RobotMap.autoY);
    			}
    			break;
    		case 3:
    			if(Robot.fuelUtil.getAtTop()){
    				Robot.fuelUtil.openFloor();
    				raisedFloor = true;
    				Timer.delay(0.5);
    				//this.time = System.currentTimeMillis();
    				step++;
    			}
    			break;
    		case 4:
    			Robot.fuelUtil.closeFloor();
    			Timer.delay(0.25);
    			Robot.fuelUtil.openFloor();
    			Timer.delay(0.25);
    			Robot.fuelUtil.closeFloor();
    			Timer.delay(0.25);
    			Robot.fuelUtil.openFloor();
    			Timer.delay(0.5);
    			Robot.fuelUtil.closeFloor();
    			Timer.delay(0.25);
    			Robot.fuelUtil.dropBasket();
    			step++;
    			reset();
    			/*if((System.currentTimeMillis() - time) >= 750){
    				if(raisedFloor){
    					Robot.fuelUtil.closeFloor();
    				}else{
    					Robot.fuelUtil.openFloor();
    				}
    				this.time = System.currentTimeMillis();
    				trials++;
    			}
    			if(trials >= 8){
    				step++;
    				reset();
    				Robot.fuelUtil.closeFloor();
    				Robot.fuelUtil.dropBasket();
    			}*/
    			break;
    		case 5:
    			//Drive to baseline
    			if(Math.abs(Robot.driveUtil.getEncoders()) >= RobotMap.autoFuelDistance3){
    				reset();
    				step++;
    			}else{
    				Robot.driveUtil.driveAuto(RobotMap.autoY);
    			}
    			break;
    		default:
    			break;
    	}
    	
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return this.done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
