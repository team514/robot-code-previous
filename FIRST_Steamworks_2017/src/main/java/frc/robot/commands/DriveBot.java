package frc.robot.commands;

import frc.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class DriveBot extends Command {

    public DriveBot() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	double z;
    	if(Robot.driveUtil.isArcade()){
    		z = Robot.oi.getRightRotation();
    	}else{
    		z = Robot.oi.getLeftRotation();
    	}
    	Robot.driveUtil.driveMecanum(Robot.oi.getRightX(),
    								 Robot.oi.getRightY(),
    								 z,
    								 false);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
